/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.syncope.utility.createservice.serviceinventory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.cxf.helpers.IOUtils;
import org.apache.syncope.common.lib.to.AnyObjectTO;
import org.apache.syncope.common.lib.to.AttrTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.InterfaceDescriptionType;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.Service;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.ServiceRole;

public class CreateServiceIntoServiceInventoryWSDL {
	private static final Logger LOG = LoggerFactory.getLogger(CreateServiceIntoServiceInventoryGidl.class);

	public static String URL = "http://localhost:9080/syncope/rest/";
	public static String USERNAME = "admin";
	public static String PASSWORD = "password";
	public static String DOMAIN = "Master";

	private static final String INPUT_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "main"
			+ File.separatorChar + "resources" + File.separatorChar;


	private static final String INTERFACE_INPUT_RESOURCES = INPUT_RESOURCES + "interface" + File.separatorChar;
	
	private static ApacheSyncopeUtilities syncopeUtilities;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		syncopeUtilities = new ApacheSyncopeUtilities(URL, USERNAME, PASSWORD, DOMAIN);

		addProviderServices();

	}

	private static void addProviderServices() {
		try {
			Service service = new Service("JourneyPlanner", "http://93.62.202.242/journeyplanner/journeyplanner/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "journeyplanner.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("JourneyPlanner_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("News", "http://93.62.202.242/news/news/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "news.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("News_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("Parking", "http://93.62.202.242/parking/parking/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "parking.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("Parking_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("Poi", "http://93.62.202.242/poi/poi/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "poi.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("Poi_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("PublicTransportation",
					"http://93.62.202.242/publictransportation/publictransportation/",
					IOUtils.readBytesFromStream(
							new FileInputStream(INTERFACE_INPUT_RESOURCES + "publictransportation.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("PublicTransportation_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("PersonalWeatherStation", "http://93.62.202.242/pws/pws/",
					IOUtils.readBytesFromStream(
							new FileInputStream(INTERFACE_INPUT_RESOURCES + "personalweatherstations.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("PersonalWeatherStation_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("Traffic", "http://93.62.202.242/traffic/traffic/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "traffic.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("Traffic_role", ""));
			syncopeUtilities.createService(service);

		} catch (IOException e) {
			LOG.error("While loading provider services", e);
		}
	}

	private static ServiceRole createRole(String roleName, String description) {
		AnyObjectTO role = syncopeUtilities.createRole(roleName, description);

		ServiceRole serviceRole = new ServiceRole();

		serviceRole.setKey(role.getKey());
		serviceRole.setName(role.getName());

		Iterator<AttrTO> iterator = role.getPlainAttrs().iterator();
		while (iterator.hasNext()) {

			AttrTO attr = iterator.next();
			if (attr.getSchema().equalsIgnoreCase(ApacheSyncopeUtilities.SERVICE_ROLE_DESCRIPTION)) {
				serviceRole.setDescription(attr.getValues().get(0));
			}

		}

		return serviceRole;

	}

}
