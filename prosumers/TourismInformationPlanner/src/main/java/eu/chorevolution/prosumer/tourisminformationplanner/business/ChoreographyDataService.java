package eu.chorevolution.prosumer.tourisminformationplanner.business;

public interface ChoreographyDataService {

	ChoreographyInstanceMessagesStore getChoreographyInstanceMessages(String choreographyID);
	
}
