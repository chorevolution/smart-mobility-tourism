﻿using System.Collections.Generic;
using Xamarin.Forms;
using STApp.Droid;
using Xamarin.Forms.Maps.Android;
using Android.Gms.Maps.Model;
using Xamarin.Forms.Maps;
using System.ComponentModel;
using STApp.CustomRenderer;
using Android.Content;

[assembly: ExportRenderer(typeof(CustomWeatherMap), typeof(WeatherCustomMapRenderer))]
namespace STApp.Droid
{
    public class WeatherCustomMapRenderer : MapRenderer
    {
        public WeatherCustomMapRenderer(Context context) : base(context)
        {

        }

        IList<Pin> customPins;
        bool isDrawn;

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var map = (CustomWeatherMap)e.NewElement;
                customPins = map.Pins;
                Control.GetMapAsync(this);
            }
        }

        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.weather));
            return marker;
        }
    }
}