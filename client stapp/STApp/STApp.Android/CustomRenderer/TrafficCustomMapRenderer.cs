﻿using System.Collections.Generic;
using Android.Content;
using Xamarin.Forms;
using STApp.CustomRenderer;
using STApp.Droid.CustomRenderer;
using Xamarin.Forms.Maps.Android;
using Xamarin.Forms.Maps;
using System.ComponentModel;
using Android.Gms.Maps.Model;

[assembly: ExportRenderer(typeof(CustomTrafficMap), typeof(TrafficCustomMapRenderer))]
namespace STApp.Droid.CustomRenderer
{
    public class TrafficCustomMapRenderer : MapRenderer
    {
        public TrafficCustomMapRenderer(Context context) : base(context)
        {

        }

        IList<Pin> customPins;
        bool isDrawn;

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var map = (CustomTrafficMap)e.NewElement;
                customPins = map.Pins;
                Control.GetMapAsync(this);
            }
        }

        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.warning));
            return marker;
        }
    }
}