﻿using Android.Content;
using Android.Gms.Maps.Model;
using STApp.CustomRenderer;
using STApp.Droid.CustomRenderer;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;

[assembly: ExportRenderer(typeof(CustomParkingMap), typeof(ParkingCustomMapRenderer))]
namespace STApp.Droid.CustomRenderer
{
    public class ParkingCustomMapRenderer : MapRenderer
    {
        public ParkingCustomMapRenderer(Context context ) : base(context)
        {

        }

        IList<Pin> customPins;
        bool isDrawn;

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var map = (CustomParkingMap)e.NewElement;
                customPins = map.Pins;
                Control.GetMapAsync(this);
            }
        }

        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.parking));
            return marker;
        }
    }
}