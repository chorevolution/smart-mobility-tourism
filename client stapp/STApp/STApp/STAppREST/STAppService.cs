﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace STApp
{
    public class STAppService : ContentPage
    {
        private HttpClient client;
        public async Task<String> GetChoreographyData(string fromLat, string fromLon, string transportType)
        {
            var restUrl = "http://192.168.150.32:9090/stapp/services/getTouristGuide?urLat=44.48&urLon=9.09&llLat=44.36&llLon=8.758&"+
                "fromLat=" + fromLat + "&fromLon=" + fromLon + "&toLat=44.410216&toLon=8.927359&transportMode=" + transportType;

            System.Diagnostics.Debug.WriteLine(restUrl);
            
            var uri = new Uri(restUrl);

            this.client = new HttpClient();
            this.client.MaxResponseContentBufferSize = 256000;

            try
            {
                var response = await this.client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return content;
                }
                return "";
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "Unable to contact the Choreography service", "OK");
                return "";
            }
        }
    }
}
