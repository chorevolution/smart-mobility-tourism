﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STApp.Models
{
    public class TrafficEvent
    {
        public string Description { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}
