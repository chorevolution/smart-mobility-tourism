﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace STApp.Models
{
    public class CustomManouevre
    {
        public string Manouevre { get; set; }
        public ImageSource ManouevreImage { get; set; }
    }
}
