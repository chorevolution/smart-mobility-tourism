﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace STApp.Models
{
    public class Segment
    {
        public int Duration { get; set; }
        public string DropInNode { get; set; }
        public string DropOffNode { get; set; }
        public string Line { get; set; }
        public string VehicleDirection { get; set; }
        public int Distance { get; set; }
        public string TransportType { get; set; }
        public List<string> Manouevres { get; set; }
        public ImageSource TransportImage { get; set; }
    }
}
