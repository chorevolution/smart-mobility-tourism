﻿namespace STApp.Models
{
    public class WeatherStation
    {
        public double Temperature { get; set; }
        public double Humidity { get; set; }
        public double Pressure { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public double Windspeed { get; set; }
        public float Rain { get; set; }
    }
}