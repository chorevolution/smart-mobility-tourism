﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace STApp.Models
{
    public class Card 
    {
        public ImageSource ImageSource { get; set; }
        public string Name { get; set; }
    }
}