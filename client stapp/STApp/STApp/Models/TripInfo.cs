﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STApp.Models
{
    public class TripInfo
    {
        public int Distance { get; set; }
        public int Duration { get; set; }
        public List<Segment> Segments { get; set; }

    }
}
