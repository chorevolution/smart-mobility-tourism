﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STApp.Models
{
    public class ParkingSpot
    {
        public string Name { get; set; }
        public string Operator { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public int Capacity { get; set; }
        public string Tariffs { get; set; }
    }
}
