﻿using Newtonsoft.Json.Linq;
using STApp.Models;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Events : ContentPage
    {
        public Events(JObject json)
        {
            InitializeComponent();

            //extract the events array
            JArray eventsArray = (JArray)json["events"];
            List<Event> events = new List<Event>();
            foreach (var e in eventsArray)
            {
                Event temp = e.ToObject<Event>();
                events.Add(temp);
            }

            //add the events to the listview
            eventList.ItemsSource = events;
        }
    }
}