﻿using STApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SegmentManouevres : ContentPage
    {
        public SegmentManouevres(Segment  s)
        {
            InitializeComponent();

            List<CustomManouevre> customManouevres = new List<CustomManouevre>();

            foreach(var m in s.Manouevres)
            {
                CustomManouevre temp = new CustomManouevre();
                temp.Manouevre = m;

                if (m.Contains("Straight"))
                    temp.ManouevreImage = "forward.png";
                else if (m.Contains("SlightRight"))
                    temp.ManouevreImage = "slight_right.png";
                else if (m.Contains("SharpRight"))
                    temp.ManouevreImage = "sharp_right.png";
                else if (m.Contains("Right"))
                    temp.ManouevreImage = "right.png";
                else if (m.Contains("SlightLeft"))
                    temp.ManouevreImage = "slight_left.png";
                else if (m.Contains("SharpLeft"))
                    temp.ManouevreImage = "sharp_left.png";
                else if (m.Contains("Left"))
                    temp.ManouevreImage = "left.png";

                customManouevres.Add(temp);
            }
            manouevres.ItemsSource = customManouevres;
        }
    }
}