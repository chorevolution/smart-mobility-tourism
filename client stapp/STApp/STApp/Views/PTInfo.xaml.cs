﻿using Newtonsoft.Json.Linq;
using STApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PTInfo : ContentPage
	{
		public PTInfo (JObject json)
		{
			InitializeComponent ();

            //extract the public transportation info array
            JArray ptInfoArray = (JArray)json["publicTransportInfo"];
            List<PT> infos = new List<PT>();
            foreach (var pt in ptInfoArray)
            {
                PT temp = pt.ToObject<PT>();
                infos.Add(temp);
            }

            //add the info to the listview
            ptList.ItemsSource = infos;
        }
	}
}