﻿using Newtonsoft.Json.Linq;
using STApp.Models;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TrafficEvents : ContentPage
	{
		public TrafficEvents (JObject json)
		{
			InitializeComponent ();
            
            //pick the traffic events and add them to the map
            JArray trafficArray = (JArray)json["trafficInfos"];
            List<TrafficEvent> trafficEvents = new List<TrafficEvent>();
            foreach (var t in trafficArray)
            {
                TrafficEvent temp = t.ToObject<TrafficEvent>();
                trafficEvents.Add(temp);
            }

            foreach (var t in trafficEvents)
            {
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = new Position(t.Lat, t.Lon),
                    Label = t.Description
                };
                TrafficMap.Pins.Add(pin);
            }
            TrafficMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(44.408188, 8.925771), Distance.FromMiles(0.5)));
        }
	}
}