﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using STApp.PlacesAPI;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class STAppHome : ContentPage
    {
        private string Lat { get; set; }
        private string Lon { get; set; }

        public STAppHome()
        {
            InitializeComponent();
            //search bar event
            MyMap.MoveToRegion(
            MapSpan.FromCenterAndRadius(
            new Xamarin.Forms.Maps.Position(44.410216, 8.927359), Distance.FromMiles(0.5)));
            search_bar.PlacesRetrieved += Search_bar_PlacesRetrieved;
        }

        private async void Results_list_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
                return;

            var prediction = (AutoCompletePrediction)e.SelectedItem;
            var place = await Places.GetPlace(prediction.Place_ID, "AIzaSyDeyOoU_dUiH-PO_HH6jrvpEa5HT_z0UA0");
            if (place != null)
            {
                search_bar.Text = "";
                search_bar.Placeholder = prediction.Description;
                this.Lat = place.Latitude.ToString(CultureInfo.InvariantCulture);
                this.Lon = place.Longitude.ToString(CultureInfo.InvariantCulture);
                NextPageButton.IsEnabled = true;
                results_list.IsVisible = false;
                //add a marker with the position
                var position = new Xamarin.Forms.Maps.Position(place.Latitude, place.Longitude);
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = position,
                    Label = "Your Position"
                };
                MyMap.Pins.Clear();
                MyMap.Pins.Add(pin);
                MyMap.MoveToRegion(
                MapSpan.FromCenterAndRadius(position, Distance.FromMiles(0.1)));
            }
        }

        //event triggered when the user write in the search bar
        private void Search_bar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.NewTextValue))
            {
                results_list.IsVisible = false;
                BarSpinner.IsVisible = true;
                BarSpinner.IsRunning = true;
            }
            else
            {
                results_list.IsVisible = true;
                BarSpinner.IsRunning = false;
                BarSpinner.IsVisible = false;
            }
        }
        
        //search bar listeners
        private void Search_bar_PlacesRetrieved(object sender, AutoCompleteResult result)
        {
            results_list.ItemsSource = result.AutoCompletePlaces;
            BarSpinner.IsRunning = false;
            BarSpinner.IsVisible = false;

            if (result.AutoCompletePlaces != null && result.AutoCompletePlaces.Count > 0)
                results_list.IsVisible = true;
        }

        private async void GetCurrentLocation()
        {
            BarSpinner.IsVisible = true;
            BarSpinner.IsRunning = true;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;
                
                if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                {
                    Plugin.Geolocator.Abstractions.Position position = await locator.GetPositionAsync(TimeSpan.FromSeconds(15), null, true);
                    this.Lat = position.Latitude.ToString(CultureInfo.InvariantCulture);
                    this.Lon = position.Longitude.ToString(CultureInfo.InvariantCulture);
                    //show into map
                    var userPosition = new Xamarin.Forms.Maps.Position(position.Latitude, position.Longitude);
                    var pin = new Pin
                    {
                        Type = PinType.Place,
                        Position = userPosition,
                        Label = "Your Position"
                    };
                    MyMap.Pins.Clear();
                    MyMap.Pins.Add(pin);
                    MyMap.MoveToRegion(
                    MapSpan.FromCenterAndRadius(userPosition, Distance.FromMiles(0.1)));

                    NextPageButton.IsEnabled = true;
                }
                else
                {
                    await DisplayAlert("Error", "Geolocation is not enabled", "OK");
                }
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "Geolocation is not enabled", "OK");
            }
            BarSpinner.IsVisible = false;
            BarSpinner.IsRunning = false;
        }
        
        //load next page
        private async void LoadNextPage(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new SelectTransport(this.Lat, this.Lon));
        }
       
    }
}