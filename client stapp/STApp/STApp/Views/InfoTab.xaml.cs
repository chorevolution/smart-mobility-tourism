﻿using Newtonsoft.Json.Linq;
using STApp.Models;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoTab : ContentPage
    {
        private string ChoreographyData { get; set; }

        public InfoTab(string data)
        {
            this.ChoreographyData = data;
            InitializeComponent();

            List<Card> serviceCards = new List<Card>();
            //load choreography response
            JObject obj = JObject.Parse(ChoreographyData);
            if (((JArray)obj["pois"]).Count > 0)
            {
                Card pois = new Card();
                pois.Name = "Points of Interest";
                pois.ImageSource = "home_poi.png";
                serviceCards.Add(pois);
            }
            if (obj["trip"] != null)
            {
                Card trip = new Card();
                trip.Name = "Trip Instructions";
                trip.ImageSource = "home_direction.png";
                serviceCards.Add(trip);
            }
            if (((JArray)obj["parkings"]).Count > 0)
            {
                Card parkings = new Card();
                parkings.Name = "Parking Spots";
                parkings.ImageSource = "home_park.png";
                serviceCards.Add(parkings);
            }
            if (((JArray)obj["trafficInfos"]).Count > 0)
            {
                Card trafficInfo = new Card();
                trafficInfo.Name = "Traffic Info";
                trafficInfo.ImageSource = "home_stoppoint.png";
                serviceCards.Add(trafficInfo);
            }
            if (((JArray)obj["publicTransportInfo"]).Count > 0)
            {
                Card publicTransport = new Card();
                publicTransport.Name = "Public Transport Info";
                publicTransport.ImageSource = "home_line.png";
                serviceCards.Add(publicTransport);
            }
            if (((JArray)obj["weatherStations"]).Count > 0)
            {
                Card weather = new Card();
                weather.Name = "Weather";
                weather.ImageSource = "home_weather.png";
                serviceCards.Add(weather);
            }
            if (((JArray)obj["events"]).Count > 0)
            {
                Card events = new Card();
                events.Name = "News";
                events.ImageSource = "home_events.png";
                serviceCards.Add(events);
            }

            services.ItemsSource = serviceCards;
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            string tappedName = ((Card)e.Item).Name;
            JObject json = JObject.Parse(ChoreographyData);
            //open different views based on tapped element
            switch (tappedName)
            {
                case "Points of Interest":
                    await Navigation.PushAsync(new PoiList(json));
                    break;

                case "News":
                    await Navigation.PushAsync(new Events(json));
                    break;

                case "Weather":
                    await Navigation.PushAsync(new Weather(json));
                    break;

                case "Parking Spots":
                    await Navigation.PushAsync(new Parkings(json));
                    break;

                case "Trip Instructions":
                    await Navigation.PushAsync(new Trip(json));
                    break;

                case "Public Transport Info":
                    await Navigation.PushAsync(new PTInfo(json));
                    break;

                case "Traffic Info":
                    await Navigation.PushAsync(new TrafficEvents(json));
                    break;

                default:
                    await DisplayAlert("", "Unimplemented view", "Ok");
                    break;

            }
        }
    }
}