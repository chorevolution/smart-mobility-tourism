﻿using Newtonsoft.Json.Linq;
using STApp.Models;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Weather : ContentPage
    {
        public Weather(JObject json)
        {
            InitializeComponent();

            //pick the weather stations and add them to the map
            JArray weatherArray = (JArray)json["weatherStations"];
            List<WeatherStation> weatherStations = new List<WeatherStation>();
            foreach (var station in weatherArray)
            {
                WeatherStation w = station.ToObject<WeatherStation>();
                weatherStations.Add(w);
            }

            foreach(var w in weatherStations)
            {
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = new Position(w.Lat, w.Lon),
                    Label = "Weather Station",
                    Address = "Temperature: " + w.Temperature + "°C, Rain: " + w.Rain + " mm/h"
                };
                WeatherMap.Pins.Add(pin);
            }
            WeatherMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(44.4282, 8.7841), Distance.FromMiles(1.0))); 
        }
    }
}