﻿using Rg.Plugins.Popup.Pages;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoadingPage : PopupPage
    {
		public LoadingPage ()
		{
			InitializeComponent ();
            BackgroundColor = Color.FromHex("#B3000000");
		}
    }
}