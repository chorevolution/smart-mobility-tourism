﻿using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectTransport : ContentPage
    {
        private string Lat { get; set; }
        private string Lon { get; set; }
        private string TransportType { get; set; }

        public SelectTransport(string lat, string lon)
        {
            this.Lat = lat;
            this.Lon = lon;
            InitializeComponent();
            //switch listeners
            WalkSwitch.Toggled += OnWalkClicked;
            CarSwitch.Toggled += OnCarClicked;
            PTSwitch.Toggled += OnTransportClicked;
        }

        private void OnWalkClicked(object sender, ToggledEventArgs e)
        {
            if (e.Value)
            {
                this.TransportType = "Walk";
                CarSwitch.IsToggled = false;
                PTSwitch.IsToggled = false;
                NextPageButton.IsEnabled = true;
            }
            else
                NextPageButton.IsEnabled = false;
        }
        
        private void OnCarClicked(object sender, ToggledEventArgs e)
        {
            if (e.Value)
            {
                this.TransportType = "Car";
                WalkSwitch.IsToggled = false;
                PTSwitch.IsToggled = false;
                NextPageButton.IsEnabled = true;
            }
            else
                NextPageButton.IsEnabled = false;
        }

        private void OnTransportClicked(object sender, ToggledEventArgs e)
        {
            if (e.Value)
            {
                this.TransportType = "PublicTransport";
                WalkSwitch.IsToggled = false;
                CarSwitch.IsToggled = false;
                NextPageButton.IsEnabled = true;
            }
            else
                NextPageButton.IsEnabled = false;
        }

        private async void LoadNextPage(object sender, EventArgs e)
        {
            //add to stack the popup page until the service responds
            var page = new LoadingPage();
            await PopupNavigation.PushAsync(page);
            //call the rest service          
            STAppService stappService = new STAppService();
            var data = await stappService.GetChoreographyData(this.Lat, this.Lon, this.TransportType);
            //remove popup page
            await PopupNavigation.PopAsync();
            //open the tabs page
            await Navigation.PushAsync(new InfoTab(data));
        }

        public static implicit operator NavigationPage(SelectTransport v)
        {
            throw new NotImplementedException();
        }
    }
}