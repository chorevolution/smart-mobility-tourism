﻿using Newtonsoft.Json.Linq;
using STApp.Models;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Trip : ContentPage
    {
        public Trip(JObject json)
        {
            InitializeComponent();

            //pick only the first trip
            JObject trip = (JObject)json["trip"];
            JArray paths = (JArray)trip["trips"];
            if(paths.Count > 0)
            {
                JObject current = (JObject)paths[0];
                TripInfo t = current.ToObject<TripInfo>();

                foreach (var s in t.Segments)
                {
                    //set the duration of the segment in minutes
                    s.Duration = s.Duration/60;
                    /* when evaluating Path=Text.Length always provide a default value for the target property (eg. Text="") 
                     * because otherwise it will be null and the trigger won't work like you expect.*/
                    if (s.DropInNode == null)
                        s.DropInNode = "";
                    if (s.DropOffNode == null)
                        s.DropOffNode = "";
                    if (s.Line == null)
                        s.Line = "";
                    if (s.VehicleDirection == null)
                        s.VehicleDirection = "";

                    //insert icon relative to segment type
                    switch (s.TransportType)
                    {
                        case "PUBLIC_TRANSPORT":
                            s.TransportImage = "publictransport.png";
                            break;
                        case "WALK":
                            s.TransportImage = "walk.png";
                            break;
                        case "CAR":
                            s.TransportImage = "car.png";
                            break;
                    }
                }
                //add segs to view
                segments.ItemsSource = t.Segments;
            }
        }

        private async void OnElemTapped(object sender, ItemTappedEventArgs e)
        {
            Segment s = (Segment)e.Item;
            //load manoevres if it's a walk/car segment
            if(s.TransportType == "WALK" || s.TransportType == "CAR")
            {
                await Navigation.PushAsync(new SegmentManouevres(s));
            }
        }
    }
}