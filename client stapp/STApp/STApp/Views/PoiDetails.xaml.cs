﻿using HtmlAgilityPack;
using STApp.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PoiDetails : ContentPage
    {
        public PoiDetails(Poi p)
        {
            InitializeComponent();

            //layout sizes
            AbsoluteLayout.SetLayoutFlags(box, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(box, new Rectangle(0, 1, 1, 0.3));

            AbsoluteLayout.SetLayoutFlags(poiImage, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(poiImage, new Rectangle(0, 0, 1, 1));

            AbsoluteLayout.SetLayoutFlags(poiTitle, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(poiTitle, new Rectangle(0.1, 0.80, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

            //extract the description from current poi
            var doc = new HtmlDocument();
            doc.LoadHtml(p.Description);
            var paragraphs = doc.DocumentNode.Descendants("p");
            var ds = "";
            foreach (var x in paragraphs)
            {
                if (x.InnerText != "")
                {
                    ds = String.Concat(ds, "\n", x.InnerText);
                }
            }
            //binding the current poi properties to view
            poiTitle.BindingContext = new
            {
                Name = p.Name
            };
            poiDescription.BindingContext = new
            {
                Description = ds
            };
            poiImage.BindingContext = new
            {
                Image = p.Image
            };
        }
    }
}