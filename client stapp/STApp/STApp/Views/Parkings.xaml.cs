﻿using Newtonsoft.Json.Linq;
using STApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Parkings : ContentPage
	{
		public Parkings (JObject json)
		{
			InitializeComponent ();

            //pick the parking spots and add them to the map
            JArray parkingArray = (JArray)json["parkings"];
            List<ParkingSpot> parkingSpots = new List<ParkingSpot>();
            foreach (var p in parkingArray)
            {
                ParkingSpot ps = p.ToObject<ParkingSpot>();
                parkingSpots.Add(ps);
            }

            foreach (var p in parkingSpots)
            {
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = new Position(p.Lat, p.Lon),
                    Label = p.Name,
                    Address = "Capacity: " + p.Capacity
                };
                ParkingMap.Pins.Add(pin);
            }
            ParkingMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(44.408188, 8.925771), Distance.FromMiles(0.5)));
        }
	}
}