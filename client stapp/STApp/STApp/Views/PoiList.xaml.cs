﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using STApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace STApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PoiList : ContentPage
    {
        public PoiList(JObject json)
        {
            InitializeComponent();

            //extract the pois array
            JArray poisArray = (JArray)json["pois"];
            List<Poi> pois = new List<Poi>();
            foreach (var poi in poisArray)
            {
                Poi p = poi.ToObject<Poi>();
                //extract the image relative to the poi
                var ds = poi["description"];
                JObject j = JObject.Parse(ds.ToString());
                var temp = j["it_IT"];
                var doc = new HtmlDocument();
                doc.LoadHtml(temp.ToString());
                p.Image = doc.DocumentNode.Descendants("img").Select(e => e.GetAttributeValue("src", null)).First();
                pois.Add(p);
            }

            //add the pois to the listview
            poiList.ItemsSource = pois;
        }

        private async void OnElemTapped(object sender, ItemTappedEventArgs e)
        {
            //load poiDetails
            await Navigation.PushAsync(new PoiDetails((Poi)e.Item));
        }
    }
}