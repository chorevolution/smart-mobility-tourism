﻿using STApp.Views;
using Xamarin.Forms;

namespace STApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new STAppHome());
        }
    }
}