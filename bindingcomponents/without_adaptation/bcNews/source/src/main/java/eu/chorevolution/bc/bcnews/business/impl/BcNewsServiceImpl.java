package eu.chorevolution.bc.bcnews.business.impl;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import eu.chorevolution.bc.bcnews.NewsService;
import eu.chorevolution.bc.bcnews.NewsServiceResponse;
import eu.chorevolution.bc.bcnews.OutputDataType;
import eu.chorevolution.bc.bcnews.InputDataType;
import eu.chorevolution.bc.bcnews.Objs;
import eu.chorevolution.bc.bcnews.business.BusinessException;
import eu.chorevolution.bc.bcnews.business.BcNewsService;

@Service
public class BcNewsServiceImpl implements BcNewsService {

	private static Logger logger = LoggerFactory.getLogger(BcNewsServiceImpl.class);

	@Override
	public OutputDataType newsService(InputDataType params) throws BusinessException {

		try {
			// build webservice URL
			
			String baseUrl = "http://srvwebri.softeco.it/t-cube/Rest/NewsService.svc/paged?";

			baseUrl = baseUrl + "lat=" + params.getLat() + "&";
			baseUrl = baseUrl + "lon=" + params.getLon() + "&";
			baseUrl = StringUtils.removeEnd(baseUrl, "&");
			

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				OutputDataType outputDataType = new OutputDataType();
				JSONObject outputDataTypeJSON = new JSONObject(sb.toString());

				if(!outputDataTypeJSON.isNull("c")) outputDataType.setC(outputDataTypeJSON.getInt("c"));
				if(!outputDataTypeJSON.isNull("objs")){
					JSONArray objsJSONArray = outputDataTypeJSON.getJSONArray("objs");
					for (int objsCount = 0; objsCount < objsJSONArray.length(); objsCount++) {
					JSONObject objsJSON = objsJSONArray.getJSONObject(objsCount);
					Objs objs = new Objs();
				if(!objsJSON.isNull("a")) objs.setA(objsJSON.getString("a"));
				if(!objsJSON.isNull("c")) objs.setC(objsJSON.getString("c"));
				if(!objsJSON.isNull("cr")) objs.setCr(objsJSON.getString("cr"));
				if(!objsJSON.isNull("did")) objs.setDid(objsJSON.getInt("did"));
				if(!objsJSON.isNull("end")) objs.setEnd(objsJSON.getString("end"));
				if(!objsJSON.isNull("id")) objs.setId(objsJSON.getInt("id"));
				if(!objsJSON.isNull("mod")) objs.setMod(objsJSON.getInt("mod"));
				if(!objsJSON.isNull("start")) objs.setStart(objsJSON.getString("start"));
				if(!objsJSON.isNull("t")) objs.setT(objsJSON.getString("t"));
					outputDataType.getObjs().add(objs);
					}
				}
				if(!outputDataTypeJSON.isNull("pi")) outputDataType.setPi(outputDataTypeJSON.getInt("pi"));
				if(!outputDataTypeJSON.isNull("ps")) outputDataType.setPs(outputDataTypeJSON.getInt("ps"));
				

				return outputDataType;


			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
