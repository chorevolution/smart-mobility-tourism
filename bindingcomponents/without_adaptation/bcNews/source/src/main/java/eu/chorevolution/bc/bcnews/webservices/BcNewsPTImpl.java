package eu.chorevolution.bc.bcnews.webservices;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bcnews.BcNewsPT;
import eu.chorevolution.bc.bcnews.InputDataType;
import eu.chorevolution.bc.bcnews.OutputDataType;
import eu.chorevolution.bc.bcnews.business.BcNewsService;

@Component(value = "BcNewsPTImpl")
public class BcNewsPTImpl implements BcNewsPT {

	private static Logger logger = LoggerFactory.getLogger(BcNewsPTImpl.class);
	
	@Autowired
	private BcNewsService service;

	@Override
	public OutputDataType newsService(InputDataType params) {
		logger.info("CALLED NewsService ON BCNEWS");
		try{
			OutputDataType response = service.newsService(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
