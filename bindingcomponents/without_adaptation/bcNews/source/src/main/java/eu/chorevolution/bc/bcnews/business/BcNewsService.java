package eu.chorevolution.bc.bcnews.business;


import eu.chorevolution.bc.bcnews.InputDataType;
import eu.chorevolution.bc.bcnews.OutputDataType;

public interface BcNewsService {

	OutputDataType newsService(InputDataType params) throws BusinessException;
}
