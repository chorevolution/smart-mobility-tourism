package eu.chorevolution.bc.bcparking.business;


import eu.chorevolution.bc.bcparking.InputDataType;
import eu.chorevolution.bc.bcparking.OutputDataType;

public interface BcParkingService {

	OutputDataType park(InputDataType params) throws BusinessException;
}
