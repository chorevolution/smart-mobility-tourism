package eu.chorevolution.bc.bcparking.webservices;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bcparking.BcParkingPT;
import eu.chorevolution.bc.bcparking.InputDataType;
import eu.chorevolution.bc.bcparking.OutputDataType;
import eu.chorevolution.bc.bcparking.business.BcParkingService;

@Component(value = "BcParkingPTImpl")
public class BcParkingPTImpl implements BcParkingPT {

	private static Logger logger = LoggerFactory.getLogger(BcParkingPTImpl.class);
	
	@Autowired
	private BcParkingService service;

	@Override
	public OutputDataType park(InputDataType params) {
		logger.info("CALLED park ON BCPARKING");
		try{
			OutputDataType response = service.park(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
