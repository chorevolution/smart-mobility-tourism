package eu.chorevolution.bc.bcparking.business.impl;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import eu.chorevolution.bc.bcparking.ParkResponse;
import eu.chorevolution.bc.bcparking.OutputDataType;
import eu.chorevolution.bc.bcparking.Park;
import eu.chorevolution.bc.bcparking.InputDataType;
import eu.chorevolution.bc.bcparking.Objs;
import eu.chorevolution.bc.bcparking.business.BusinessException;
import eu.chorevolution.bc.bcparking.business.BcParkingService;

@Service
public class BcParkingServiceImpl implements BcParkingService {

	private static Logger logger = LoggerFactory.getLogger(BcParkingServiceImpl.class);

	@Override
	public OutputDataType park(InputDataType params) throws BusinessException {

		try {
			// build webservice URL
			
			String baseUrl = "http://srvwebri.softeco.it/t-cube/Rest/park.svc/pagedparking?";

			baseUrl = baseUrl + "llLat=" + params.getLlLat() + "&";
			baseUrl = baseUrl + "llLon=" + params.getLlLon() + "&";
			baseUrl = baseUrl + "urLat=" + params.getUrLat() + "&";
			baseUrl = baseUrl + "urLon=" + params.getUrLon() + "&";
			baseUrl = StringUtils.removeEnd(baseUrl, "&");
			

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				OutputDataType outputDataType = new OutputDataType();
				JSONObject outputDataTypeJSON = new JSONObject(sb.toString());

				if(!outputDataTypeJSON.isNull("c")) outputDataType.setC(outputDataTypeJSON.getInt("c"));
				if(!outputDataTypeJSON.isNull("objs")){
					JSONArray objsJSONArray = outputDataTypeJSON.getJSONArray("objs");
					for (int objsCount = 0; objsCount < objsJSONArray.length(); objsCount++) {
					JSONObject objsJSON = objsJSONArray.getJSONObject(objsCount);
					Objs objs = new Objs();
				if(!objsJSON.isNull("c")) objs.setC(objsJSON.getInt("c"));
				if(!objsJSON.isNull("cp")) objs.setCp(objsJSON.getInt("cp"));
				if(!objsJSON.isNull("d")) objs.setD(objsJSON.getInt("d"));
				if(!objsJSON.isNull("dc")) objs.setDc(objsJSON.getInt("dc"));
				if(!objsJSON.isNull("dt")) objs.setDt(objsJSON.getBoolean("dt"));
				if(!objsJSON.isNull("id")) objs.setId(objsJSON.getString("id"));
				if(!objsJSON.isNull("n")) objs.setN(objsJSON.getString("n"));
				if(!objsJSON.isNull("op")) objs.setOp(objsJSON.getString("op"));
				if(!objsJSON.isNull("rc")) objs.setRc(objsJSON.getInt("rc"));
				if(!objsJSON.isNull("st")) objs.setSt(objsJSON.getInt("st"));
				if(!objsJSON.isNull("t")) objs.setT(objsJSON.getBoolean("t"));
				if(!objsJSON.isNull("ta")) objs.setTa(objsJSON.getString("ta"));
				if(!objsJSON.isNull("ty")) objs.setTy(objsJSON.getInt("ty"));
				if(!objsJSON.isNull("v")) objs.setV(objsJSON.getBoolean("v"));
				if(!objsJSON.isNull("x")) objs.setX(objsJSON.getDouble("x"));
				if(!objsJSON.isNull("y")) objs.setY(objsJSON.getDouble("y"));
					outputDataType.getObjs().add(objs);
					}
				}
				

				return outputDataType;


			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
