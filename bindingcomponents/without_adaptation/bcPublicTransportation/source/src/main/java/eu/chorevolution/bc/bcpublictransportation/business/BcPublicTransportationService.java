package eu.chorevolution.bc.bcpublictransportation.business;

import java.util.List;

import eu.chorevolution.bc.bcpublictransportation.InputDataType;
import eu.chorevolution.bc.bcpublictransportation.OutputDataType;

public interface BcPublicTransportationService {

	List<OutputDataType> publicTransportation(InputDataType params) throws BusinessException;
}
