package eu.chorevolution.bc.bcpublictransportation.webservices;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bcpublictransportation.BcPublicTransportationPT;
import eu.chorevolution.bc.bcpublictransportation.InputDataType;
import eu.chorevolution.bc.bcpublictransportation.OutputDataType;
import eu.chorevolution.bc.bcpublictransportation.business.BcPublicTransportationService;

@Component(value = "BcPublicTransportationPTImpl")
public class BcPublicTransportationPTImpl implements BcPublicTransportationPT {

	private static Logger logger = LoggerFactory.getLogger(BcPublicTransportationPTImpl.class);
	
	@Autowired
	private BcPublicTransportationService service;

	@Override
	public List<OutputDataType> publicTransportation(InputDataType params) {
		logger.info("CALLED publicTransportation ON BCPUBLICTRANSPORTATION");
		try{
			List<OutputDataType> response = service.publicTransportation(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
