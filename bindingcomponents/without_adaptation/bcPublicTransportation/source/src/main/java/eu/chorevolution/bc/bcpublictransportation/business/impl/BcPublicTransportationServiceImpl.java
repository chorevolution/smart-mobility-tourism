package eu.chorevolution.bc.bcpublictransportation.business.impl;

import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import eu.chorevolution.bc.bcpublictransportation.PublicTransportation;
import eu.chorevolution.bc.bcpublictransportation.OutputDataType;
import eu.chorevolution.bc.bcpublictransportation.PublicTransportationResponse;
import eu.chorevolution.bc.bcpublictransportation.InputDataType;
import eu.chorevolution.bc.bcpublictransportation.business.BusinessException;
import eu.chorevolution.bc.bcpublictransportation.business.BcPublicTransportationService;

@Service
public class BcPublicTransportationServiceImpl implements BcPublicTransportationService {

	private static Logger logger = LoggerFactory.getLogger(BcPublicTransportationServiceImpl.class);

	@Override
	public List<OutputDataType> publicTransportation(InputDataType params) throws BusinessException {

		try {
			// build webservice URL
			
			String baseUrl = "http://cho-noauth-srv.e-mixer.com/services/Rest/newsService.svc/latest?";

			baseUrl = baseUrl + "lat=" + params.getLat() + "&";
			baseUrl = baseUrl + "lon=" + params.getLon() + "&";
			baseUrl = StringUtils.removeEnd(baseUrl, "&");
			

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				List<OutputDataType> outputDataTypes = new ArrayList<>();
				JSONArray outputDataTypeJSONArray = new JSONArray(sb.toString());
				for (int outputDataTypeCount=0; outputDataTypeCount<outputDataTypeJSONArray.length(); outputDataTypeCount++) {
					JSONObject outputDataTypeJSON = outputDataTypeJSONArray.getJSONObject(outputDataTypeCount);
					OutputDataType outputDataType = new OutputDataType();

				if(!outputDataTypeJSON.isNull("c")) outputDataType.setC(outputDataTypeJSON.getString("c"));
				if(!outputDataTypeJSON.isNull("did")) outputDataType.setDid(outputDataTypeJSON.getInt("did"));
				if(!outputDataTypeJSON.isNull("id")) outputDataType.setId(outputDataTypeJSON.getInt("id"));
				if(!outputDataTypeJSON.isNull("mod")) outputDataType.setMod(outputDataTypeJSON.getInt("mod"));
				if(!outputDataTypeJSON.isNull("start")) outputDataType.setStart(outputDataTypeJSON.getString("start"));
				if(!outputDataTypeJSON.isNull("t")) outputDataType.setT(outputDataTypeJSON.getString("t"));
				

					outputDataTypes.add(outputDataType);
				}
				return outputDataTypes;


			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
