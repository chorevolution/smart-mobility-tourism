package eu.chorevolution.bc.bctraffic.business;

import java.util.List;

import eu.chorevolution.bc.bctraffic.InputDataType;
import eu.chorevolution.bc.bctraffic.OutputDataType;

public interface BcTrafficService {

	List<OutputDataType> getMetadataInArea(InputDataType params) throws BusinessException;
}
