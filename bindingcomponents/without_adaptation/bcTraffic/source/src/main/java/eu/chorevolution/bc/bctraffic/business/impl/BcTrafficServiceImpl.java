package eu.chorevolution.bc.bctraffic.business.impl;

import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import eu.chorevolution.bc.bctraffic.GetMetadataInAreaResponse;
import eu.chorevolution.bc.bctraffic.GetMetadataInArea;
import eu.chorevolution.bc.bctraffic.OutputDataType;
import eu.chorevolution.bc.bctraffic.InputDataType;
import eu.chorevolution.bc.bctraffic.business.BusinessException;
import eu.chorevolution.bc.bctraffic.business.BcTrafficService;

@Service
public class BcTrafficServiceImpl implements BcTrafficService {

	private static Logger logger = LoggerFactory.getLogger(BcTrafficServiceImpl.class);

	@Override
	public List<OutputDataType> getMetadataInArea(InputDataType params) throws BusinessException {

		try {
			// build webservice URL
			
			String baseUrl = "http://cho-noauth-srv.e-mixer.com/services/Rest/trafficService.svc/scheduled?";

			baseUrl = baseUrl + "urLat=" + params.getUrLat() + "&";
			baseUrl = baseUrl + "urLon=" + params.getUrLon() + "&";
			baseUrl = baseUrl + "llLat=" + params.getLlLat() + "&";
			baseUrl = baseUrl + "llLon=" + params.getLlLon() + "&";
			baseUrl = StringUtils.removeEnd(baseUrl, "&");
			

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				List<OutputDataType> outputDataTypes = new ArrayList<>();
				JSONArray outputDataTypeJSONArray = new JSONArray(sb.toString());
				for (int outputDataTypeCount=0; outputDataTypeCount<outputDataTypeJSONArray.length(); outputDataTypeCount++) {
					JSONObject outputDataTypeJSON = outputDataTypeJSONArray.getJSONObject(outputDataTypeCount);
					OutputDataType outputDataType = new OutputDataType();

				if(!outputDataTypeJSON.isNull("d")) outputDataType.setD(outputDataTypeJSON.getInt("d"));
				if(!outputDataTypeJSON.isNull("desc")) outputDataType.setDesc(outputDataTypeJSON.getString("desc"));
				if(!outputDataTypeJSON.isNull("dty")) outputDataType.setDty(outputDataTypeJSON.getInt("dty"));
				if(!outputDataTypeJSON.isNull("e")) outputDataType.setE(outputDataTypeJSON.getString("e"));
				if(!outputDataTypeJSON.isNull("l")) outputDataType.setL(outputDataTypeJSON.getString("l"));
				if(!outputDataTypeJSON.isNull("n")) outputDataType.setN(outputDataTypeJSON.getString("n"));
				if(!outputDataTypeJSON.isNull("p")) outputDataType.setP(outputDataTypeJSON.getString("p"));
				if(!outputDataTypeJSON.isNull("s")) outputDataType.setS(outputDataTypeJSON.getString("s"));
				if(!outputDataTypeJSON.isNull("ty")) outputDataType.setTy(outputDataTypeJSON.getInt("ty"));
				if(!outputDataTypeJSON.isNull("v")) outputDataType.setV(outputDataTypeJSON.getString("v"));
				if(!outputDataTypeJSON.isNull("x")) outputDataType.setX(outputDataTypeJSON.getDouble("x"));
				if(!outputDataTypeJSON.isNull("y")) outputDataType.setY(outputDataTypeJSON.getDouble("y"));
				

					outputDataTypes.add(outputDataType);
				}
				return outputDataTypes;


			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
