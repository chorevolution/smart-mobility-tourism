package eu.chorevolution.bc.bctraffic.webservices;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bctraffic.BcTrafficPT;
import eu.chorevolution.bc.bctraffic.InputDataType;
import eu.chorevolution.bc.bctraffic.OutputDataType;
import eu.chorevolution.bc.bctraffic.business.BcTrafficService;

@Component(value = "BcTrafficPTImpl")
public class BcTrafficPTImpl implements BcTrafficPT {

	private static Logger logger = LoggerFactory.getLogger(BcTrafficPTImpl.class);
	
	@Autowired
	private BcTrafficService service;

	@Override
	public List<OutputDataType> getMetadataInArea(InputDataType params) {
		logger.info("CALLED getMetadataInArea ON BCTRAFFIC");
		try{
			List<OutputDataType> response = service.getMetadataInArea(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
