package eu.chorevolution.bc.bcpoi.business;


import eu.chorevolution.bc.bcpoi.InputDataType;
import eu.chorevolution.bc.bcpoi.OutputDataType;

public interface BcPoiService {

	OutputDataType pws(InputDataType params) throws BusinessException;
}
