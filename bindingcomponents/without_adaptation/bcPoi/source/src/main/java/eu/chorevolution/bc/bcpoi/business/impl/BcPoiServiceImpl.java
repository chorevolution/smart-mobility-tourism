package eu.chorevolution.bc.bcpoi.business.impl;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import eu.chorevolution.bc.bcpoi.P;
import eu.chorevolution.bc.bcpoi.OutputDataType;
import eu.chorevolution.bc.bcpoi.PwsResponse;
import eu.chorevolution.bc.bcpoi.Pws;
import eu.chorevolution.bc.bcpoi.InputDataType;
import eu.chorevolution.bc.bcpoi.Objs;
import eu.chorevolution.bc.bcpoi.business.BusinessException;
import eu.chorevolution.bc.bcpoi.business.BcPoiService;

@Service
public class BcPoiServiceImpl implements BcPoiService {

	private static Logger logger = LoggerFactory.getLogger(BcPoiServiceImpl.class);

	@Override
	public OutputDataType pws(InputDataType params) throws BusinessException {

		try {
			// build webservice URL
			
			String baseUrl = "http://srvwebri.softeco.it/t-cube/Rest/pws.svc/pageditems?";

			baseUrl = baseUrl + "llLat=" + params.getLlLat() + "&";
			baseUrl = baseUrl + "llLon=" + params.getLlLon() + "&";
			baseUrl = baseUrl + "urLat=" + params.getUrLat() + "&";
			baseUrl = baseUrl + "urLon=" + params.getUrLon() + "&";
			baseUrl = baseUrl + "type=" + params.getType() + "&";
			baseUrl = StringUtils.removeEnd(baseUrl, "&");
			

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				OutputDataType outputDataType = new OutputDataType();
				JSONObject outputDataTypeJSON = new JSONObject(sb.toString());

				if(!outputDataTypeJSON.isNull("c")) outputDataType.setC(outputDataTypeJSON.getInt("c"));
				if(!outputDataTypeJSON.isNull("objs")){
					JSONArray objsJSONArray = outputDataTypeJSON.getJSONArray("objs");
					for (int objsCount = 0; objsCount < objsJSONArray.length(); objsCount++) {
					JSONObject objsJSON = objsJSONArray.getJSONObject(objsCount);
					Objs objs = new Objs();
				if(!objsJSON.isNull("id")) objs.setId(objsJSON.getString("id"));
				if(!objsJSON.isNull("gt")) objs.setGt(objsJSON.getInt("gt"));
				if(!objsJSON.isNull("t")) objs.setT(objsJSON.getInt("t"));
				if(!objsJSON.isNull("p")){
					JSONObject pJSON = objsJSON.getJSONObject("p");
					P p = new P();
				if(!pJSON.isNull("t")) p.setT(pJSON.getInt("t"));
				if(!pJSON.isNull("x")) p.setX(pJSON.getDouble("x"));
				if(!pJSON.isNull("y")) p.setY(pJSON.getDouble("y"));
					objs.setP(p);
				}
				if(!objsJSON.isNull("n")) objs.setN(objsJSON.getString("n"));
				if(!objsJSON.isNull("ds")) objs.setDs(objsJSON.getString("ds"));
				if(!objsJSON.isNull("u")) objs.setU(objsJSON.getString("u"));
				if(!objsJSON.isNull("adr")) objs.setAdr(objsJSON.getString("adr"));
				if(!objsJSON.isNull("ed")) objs.setEd(objsJSON.getString("ed"));
					outputDataType.getObjs().add(objs);
					}
				}
				if(!outputDataTypeJSON.isNull("pi")) outputDataType.setPi(outputDataTypeJSON.getInt("pi"));
				if(!outputDataTypeJSON.isNull("ps")) outputDataType.setPs(outputDataTypeJSON.getInt("ps"));
				

				return outputDataType;


			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
