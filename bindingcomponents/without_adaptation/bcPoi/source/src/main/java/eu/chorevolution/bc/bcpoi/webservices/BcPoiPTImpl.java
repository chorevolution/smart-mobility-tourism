package eu.chorevolution.bc.bcpoi.webservices;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bcpoi.BcPoiPT;
import eu.chorevolution.bc.bcpoi.InputDataType;
import eu.chorevolution.bc.bcpoi.OutputDataType;
import eu.chorevolution.bc.bcpoi.business.BcPoiService;

@Component(value = "BcPoiPTImpl")
public class BcPoiPTImpl implements BcPoiPT {

	private static Logger logger = LoggerFactory.getLogger(BcPoiPTImpl.class);
	
	@Autowired
	private BcPoiService service;

	@Override
	public OutputDataType pws(InputDataType params) {
		logger.info("CALLED pws ON BCPOI");
		try{
			OutputDataType response = service.pws(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
