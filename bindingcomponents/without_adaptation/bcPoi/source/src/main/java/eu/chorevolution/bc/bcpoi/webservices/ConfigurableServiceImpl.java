package eu.chorevolution.bc.bcpoi.webservices;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bcpoi.baseservice.ConfigurableService;
import eu.chorevolution.bc.bcpoi.util.SetInvocationAddressUtils;

@Component(value = "ConfigurableServiceImpl")
public class ConfigurableServiceImpl implements ConfigurableService{

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurableServiceImpl.class);
	
	@Override
	public void setInvocationAddress(String arg0, String arg1, List<String> arg2) {
		
		LOGGER.info("CALLED setInvocationAddress ON bcPoi");
		SetInvocationAddressUtils.storeArtifactEndpointData(arg0, arg1, arg2);
	}

}
