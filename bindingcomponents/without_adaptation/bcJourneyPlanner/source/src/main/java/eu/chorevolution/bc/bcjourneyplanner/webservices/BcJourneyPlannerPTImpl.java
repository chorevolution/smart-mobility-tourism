package eu.chorevolution.bc.bcjourneyplanner.webservices;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bcjourneyplanner.BcJourneyPlannerPT;
import eu.chorevolution.bc.bcjourneyplanner.InputDataType;
import eu.chorevolution.bc.bcjourneyplanner.OutputDataType;
import eu.chorevolution.bc.bcjourneyplanner.business.BcJourneyPlannerService;

@Component(value = "BcJourneyPlannerPTImpl")
public class BcJourneyPlannerPTImpl implements BcJourneyPlannerPT {

	private static Logger logger = LoggerFactory.getLogger(BcJourneyPlannerPTImpl.class);
	
	@Autowired
	private BcJourneyPlannerService service;

	@Override
	public OutputDataType journeyPlanningService(InputDataType params) {
		logger.info("CALLED JourneyPlanningService ON BCJOURNEYPLANNER");
		try{
			OutputDataType response = service.journeyPlanningService(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
