package eu.chorevolution.bc.bcjourneyplanner.business;


import eu.chorevolution.bc.bcjourneyplanner.InputDataType;
import eu.chorevolution.bc.bcjourneyplanner.OutputDataType;

public interface BcJourneyPlannerService {

	OutputDataType journeyPlanningService(InputDataType params) throws BusinessException;
}
