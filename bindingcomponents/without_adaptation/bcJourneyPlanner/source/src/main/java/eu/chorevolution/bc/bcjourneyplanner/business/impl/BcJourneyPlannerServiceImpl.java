package eu.chorevolution.bc.bcjourneyplanner.business.impl;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import eu.chorevolution.bc.bcjourneyplanner.Ps;
import eu.chorevolution.bc.bcjourneyplanner.T;
import eu.chorevolution.bc.bcjourneyplanner.Vts;
import eu.chorevolution.bc.bcjourneyplanner.F;
import eu.chorevolution.bc.bcjourneyplanner.V;
import eu.chorevolution.bc.bcjourneyplanner.JourneyPlanningServiceResponse;
import eu.chorevolution.bc.bcjourneyplanner.Segs;
import eu.chorevolution.bc.bcjourneyplanner.JourneyPlanningService;
import eu.chorevolution.bc.bcjourneyplanner.OutputDataType;
import eu.chorevolution.bc.bcjourneyplanner.InputDataType;
import eu.chorevolution.bc.bcjourneyplanner.Po;
import eu.chorevolution.bc.bcjourneyplanner.Ts;
import eu.chorevolution.bc.bcjourneyplanner.business.BusinessException;
import eu.chorevolution.bc.bcjourneyplanner.business.BcJourneyPlannerService;

@Service
public class BcJourneyPlannerServiceImpl implements BcJourneyPlannerService {

	private static Logger logger = LoggerFactory.getLogger(BcJourneyPlannerServiceImpl.class);

	@Override
	public OutputDataType journeyPlanningService(InputDataType params) throws BusinessException {

		try {
			// build webservice URL
			
			String baseUrl = "http://ge-srv.e-mixer.com/Rest/JourneyPlanningService.svc/trips?";

			baseUrl = baseUrl + "fromLat=" + params.getFromLat() + "&";
			baseUrl = baseUrl + "fromLon=" + params.getFromLon() + "&";
			baseUrl = baseUrl + "toLat=" + params.getToLat() + "&";
			baseUrl = baseUrl + "toLon=" + params.getToLon() + "&";
			baseUrl = baseUrl + "modes=" + params.getModes() + "&";
			baseUrl = StringUtils.removeEnd(baseUrl, "&");
			

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				OutputDataType outputDataType = new OutputDataType();
				JSONObject outputDataTypeJSON = new JSONObject(sb.toString());

				if(!outputDataTypeJSON.isNull("f")){
					JSONObject fJSON = outputDataTypeJSON.getJSONObject("f");
					F f = new F();
				if(!fJSON.isNull("n")) f.setN(fJSON.getString("n"));
				if(!fJSON.isNull("x")) f.setX(fJSON.getDouble("x"));
				if(!fJSON.isNull("y")) f.setY(fJSON.getDouble("y"));
					outputDataType.setF(f);
				}
				if(!outputDataTypeJSON.isNull("id")) outputDataType.setId(outputDataTypeJSON.getString("id"));
				if(!outputDataTypeJSON.isNull("t")){
					JSONObject tJSON = outputDataTypeJSON.getJSONObject("t");
					T t = new T();
				if(!tJSON.isNull("n")) t.setN(tJSON.getString("n"));
				if(!tJSON.isNull("x")) t.setX(tJSON.getDouble("x"));
				if(!tJSON.isNull("y")) t.setY(tJSON.getDouble("y"));
					outputDataType.setT(t);
				}
				if(!outputDataTypeJSON.isNull("ts")){
					JSONArray tsJSONArray = outputDataTypeJSON.getJSONArray("ts");
					for (int tsCount = 0; tsCount < tsJSONArray.length(); tsCount++) {
					JSONObject tsJSON = tsJSONArray.getJSONObject(tsCount);
					Ts ts = new Ts();
				if(!tsJSON.isNull("co")) ts.setCo(tsJSON.getInt("co"));
				if(!tsJSON.isNull("d")) ts.setD(tsJSON.getInt("d"));
				if(!tsJSON.isNull("du")) ts.setDu(tsJSON.getInt("du"));
				if(!tsJSON.isNull("dwf")) ts.setDwf(tsJSON.getBoolean("dwf"));
				if(!tsJSON.isNull("e")) ts.setE(tsJSON.getInt("e"));
				if(!tsJSON.isNull("m")) ts.setM(tsJSON.getInt("m"));
				if(!tsJSON.isNull("s")) ts.setS(tsJSON.getInt("s"));
				if(!tsJSON.isNull("segs")){
					JSONArray segsJSONArray = tsJSON.getJSONArray("segs");
					for (int segsCount = 0; segsCount < segsJSONArray.length(); segsCount++) {
					JSONObject segsJSON = segsJSONArray.getJSONObject(segsCount);
					Segs segs = new Segs();
				if(!segsJSON.isNull("d")) segs.setD(segsJSON.getInt("d"));
				if(!segsJSON.isNull("du")) segs.setDu(segsJSON.getInt("du"));
				if(!segsJSON.isNull("po")){
					JSONObject poJSON = segsJSON.getJSONObject("po");
					Po po = new Po();
				if(!poJSON.isNull("c")) po.setC(poJSON.getString("c"));
				if(!poJSON.isNull("ep")) po.setEp(poJSON.getString("ep"));
					segs.setPo(po);
				}
				if(!segsJSON.isNull("ps")){
					JSONArray psJSONArray = segsJSON.getJSONArray("ps");
					for (int psCount = 0; psCount < psJSONArray.length(); psCount++) {
					JSONObject psJSON = psJSONArray.getJSONObject(psCount);
					Ps ps = new Ps();
				if(!psJSON.isNull("b")) ps.setB(psJSON.getInt("b"));
				if(!psJSON.isNull("d")) ps.setD(psJSON.getInt("d"));
				if(!psJSON.isNull("f")) ps.setF(psJSON.getString("f"));
				if(!psJSON.isNull("m")) ps.setM(psJSON.getInt("m"));
				if(!psJSON.isNull("mc")) ps.setMc(psJSON.getBoolean("mc"));
				if(!psJSON.isNull("rtc")) ps.setRtc(psJSON.getInt("rtc"));
				if(!psJSON.isNull("x")) ps.setX(psJSON.getDouble("x"));
				if(!psJSON.isNull("y")) ps.setY(psJSON.getDouble("y"));
					segs.getPs().add(ps);
					}
				}
				if(!segsJSON.isNull("t")) segs.setT(segsJSON.getInt("t"));
				if(!segsJSON.isNull("v")){
					JSONObject vJSON = segsJSON.getJSONObject("v");
					V v = new V();
				if(!vJSON.isNull("d")) v.setD(vJSON.getString("d"));
				if(!vJSON.isNull("e")) v.setE(vJSON.getString("e"));
				if(!vJSON.isNull("eid")) v.setEid(vJSON.getString("eid"));
				if(!vJSON.isNull("et")) v.setEt(vJSON.getInt("et"));
				if(!vJSON.isNull("l")) v.setL(vJSON.getString("l"));
				if(!vJSON.isNull("s")) v.setS(vJSON.getString("s"));
				if(!vJSON.isNull("sid")) v.setSid(vJSON.getString("sid"));
				if(!vJSON.isNull("st")) v.setSt(vJSON.getInt("st"));
				if(!vJSON.isNull("t")) v.setT(vJSON.getString("t"));
				if(!vJSON.isNull("vss")) v.setVss(vJSON.getString("vss"));
					segs.setV(v);
				}
				if(!segsJSON.isNull("vts")){
					JSONArray vtsJSONArray = segsJSON.getJSONArray("vts");
					for (int vtsCount = 0; vtsCount < vtsJSONArray.length(); vtsCount++) {
					JSONObject vtsJSON = vtsJSONArray.getJSONObject(vtsCount);
					Vts vts = new Vts();
				if(!vtsJSON.isNull("u")) vts.setU(vtsJSON.getString("u"));
				if(!vtsJSON.isNull("p")) vts.setP(vtsJSON.getString("p"));
				if(!vtsJSON.isNull("d")) vts.setD(vtsJSON.getString("d"));
					segs.getVts().add(vts);
					}
				}
					ts.getSegs().add(segs);
					}
				}
					outputDataType.getTs().add(ts);
					}
				}
				

				return outputDataType;


			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
