package eu.chorevolution.bc.bcpersonalweatherstations.business;

import java.util.List;

import eu.chorevolution.bc.bcersonalweatherstations.InputDataType;
import eu.chorevolution.bc.bcersonalweatherstations.OutputDataType;

public interface BcPersonalWeatherStationsService {

	List<OutputDataType> itemService(InputDataType params) throws BusinessException;
}
