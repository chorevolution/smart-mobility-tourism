package eu.chorevolution.bc.bcpersonalweatherstations.business.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import eu.chorevolution.bc.bcersonalweatherstations.InputDataType;
import eu.chorevolution.bc.bcersonalweatherstations.OutputDataType;
import eu.chorevolution.bc.bcersonalweatherstations.P;
import eu.chorevolution.bc.bcpersonalweatherstations.business.BcPersonalWeatherStationsService;
import eu.chorevolution.bc.bcpersonalweatherstations.business.BusinessException;

@Service
public class BcPersonalWeatherStationsServiceImpl implements BcPersonalWeatherStationsService {

	private static Logger logger = LoggerFactory.getLogger(BcPersonalWeatherStationsServiceImpl.class);

	@Value("#{cfgproperties.pwd}")
	private String pwd;

	@Override
	public List<OutputDataType> itemService(InputDataType params) throws BusinessException {

		try {
			// build webservice URL
			
			String baseUrl = "http://cho-srv.e-mixer.com/services/Rest/ItemService.svc/items?";

			baseUrl = baseUrl + "urLat=" + params.getUrLat() + "&";
			baseUrl = baseUrl + "urLon=" + params.getUrLon() + "&";
			baseUrl = baseUrl + "llLat=" + params.getLlLat() + "&";
			baseUrl = baseUrl + "llLon=" + params.getLlLon() + "&";
			baseUrl = baseUrl + "type=" + params.getType() + "&";
			baseUrl = baseUrl + "s=" + SecurityHelper.getDynamicPassword(pwd) + "&";
			baseUrl = StringUtils.removeEnd(baseUrl, "&");
			

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				List<OutputDataType> outputDataTypes = new ArrayList<>();
				JSONArray outputDataTypeJSONArray = new JSONArray(sb.toString());
				for (int outputDataTypeCount=0; outputDataTypeCount<outputDataTypeJSONArray.length(); outputDataTypeCount++) {
					JSONObject outputDataTypeJSON = outputDataTypeJSONArray.getJSONObject(outputDataTypeCount);
					OutputDataType outputDataType = new OutputDataType();

				if(!outputDataTypeJSON.isNull("id")) outputDataType.setId(outputDataTypeJSON.getString("id"));
				if(!outputDataTypeJSON.isNull("gt")) outputDataType.setGt(outputDataTypeJSON.getInt("gt"));
				if(!outputDataTypeJSON.isNull("t")) outputDataType.setT(outputDataTypeJSON.getInt("t"));
				if(!outputDataTypeJSON.isNull("p")){
					JSONObject pJSON = outputDataTypeJSON.getJSONObject("p");
					P p = new P();
				if(!pJSON.isNull("t")) p.setT(pJSON.getInt("t"));
				if(!pJSON.isNull("x")) p.setX(pJSON.getDouble("x"));
				if(!pJSON.isNull("y")) p.setY(pJSON.getDouble("y"));
					outputDataType.setP(p);
				}
				if(!outputDataTypeJSON.isNull("n")) outputDataType.setN(outputDataTypeJSON.getString("n"));
				if(!outputDataTypeJSON.isNull("ds")) outputDataType.setDs(outputDataTypeJSON.getString("ds"));
				if(!outputDataTypeJSON.isNull("u")) outputDataType.setU(outputDataTypeJSON.getString("u"));
				if(!outputDataTypeJSON.isNull("adr")) outputDataType.setAdr(outputDataTypeJSON.getString("adr"));
				

					outputDataTypes.add(outputDataType);
				}
				return outputDataTypes;


			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
