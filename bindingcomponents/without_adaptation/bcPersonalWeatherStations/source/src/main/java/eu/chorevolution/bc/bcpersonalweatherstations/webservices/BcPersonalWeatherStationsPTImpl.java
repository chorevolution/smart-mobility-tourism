package eu.chorevolution.bc.bcpersonalweatherstations.webservices;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.bc.bcersonalweatherstations.BcPersonalWeatherStationsPT;
import eu.chorevolution.bc.bcersonalweatherstations.InputDataType;
import eu.chorevolution.bc.bcersonalweatherstations.OutputDataType;
import eu.chorevolution.bc.bcpersonalweatherstations.business.BcPersonalWeatherStationsService;

@Component(value = "BcPersonalWeatherStationsPTImpl")
public class BcPersonalWeatherStationsPTImpl implements BcPersonalWeatherStationsPT {

	private static Logger logger = LoggerFactory.getLogger(BcPersonalWeatherStationsPTImpl.class);
	
	@Autowired
	private BcPersonalWeatherStationsService service;

	@Override
	public List<OutputDataType> itemService(InputDataType params) {
		logger.info("CALLED ItemService ON BCPWS");
		try{
			List<OutputDataType> response = service.itemService(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
