package it.softeco.chorevolution.smt.stapprest;

import java.net.URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import it.softeco.chorevolution.smt.stapprest.util.SetInvocationAddressUtils;

@Controller
public class StappREST {

	private static Logger logger = LoggerFactory.getLogger(StappREST.class);

	@GET
	@Produces("text/plain")
	@Path("/getTouristGuide")
	public String getTouristGuide(@QueryParam("urLat") double urLat, @QueryParam("urLon") double urLon,
			@QueryParam("llLat") double llLat, @QueryParam("llLon") double llLon, @QueryParam("fromLat") double fromLat,
			@QueryParam("fromLon") double fromLon, @QueryParam("toLat") double toLat, @QueryParam("toLon") double toLon,
			@QueryParam("transportMode") String transportMode) {

		logger.info("CALLED getTouristGuide ON STAPP");

		try {
			URL WSDL_URL = SetInvocationAddressUtils.getArtifactEndpointURLFromRole("STApp");
			STApp cdstAppService = null;
			if (WSDL_URL == null) {
				cdstAppService = new STApp();
			} else {
				cdstAppService = new STApp(WSDL_URL);
			}
			
			STAppPortType cdstAppPT = cdstAppService.getSTAppPort();

			GetTouristicGuideRequest request = new GetTouristicGuideRequest();
			request.setFromLat(fromLat);
			request.setFromLon(fromLon);
			request.setToLat(toLat);
			request.setToLon(toLon);
			request.setUpperRightLat(urLat);
			request.setUpperRightLon(urLon);
			request.setLowerLeftLat(llLat);
			request.setLowerLeftLon(llLon);
			request.setTransportMode(Modes.fromValue(transportMode));

			GetTouristicGuideRequestType requestType = new GetTouristicGuideRequestType();
			requestType.setMessageData(request);

			GetTouristicGuideResponse response = cdstAppPT.getTouristGuide(requestType);

			// convert the planResponse to json
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(response);
			return json;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}
}