package it.softeco.chorevolution.smt.parking.business.impl.dummy;

import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.parking.ParkingRequest;
import it.softeco.chorevolution.smt.parking.ParkingResponse;
import it.softeco.chorevolution.smt.parking.business.BusinessException;
import it.softeco.chorevolution.smt.parking.business.ParkingService;

@Service
public class DummyParkingServiceImpl implements ParkingService {

	@Override
	public ParkingResponse getParking(ParkingRequest params) throws BusinessException {
		ParkingResponse response = new ParkingResponse();
		return response;
	}
}
