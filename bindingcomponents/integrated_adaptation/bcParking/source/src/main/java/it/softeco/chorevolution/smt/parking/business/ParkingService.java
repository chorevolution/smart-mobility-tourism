package it.softeco.chorevolution.smt.parking.business;

import it.softeco.chorevolution.smt.parking.ParkingRequest;
import it.softeco.chorevolution.smt.parking.ParkingResponse;

public interface ParkingService {

	ParkingResponse getParking(ParkingRequest params) throws BusinessException;
}
