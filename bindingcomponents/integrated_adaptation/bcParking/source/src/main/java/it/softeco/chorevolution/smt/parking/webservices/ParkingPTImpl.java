package it.softeco.chorevolution.smt.parking.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.parking.ParkingPT;
import it.softeco.chorevolution.smt.parking.ParkingRequest;
import it.softeco.chorevolution.smt.parking.ParkingResponse;
import it.softeco.chorevolution.smt.parking.business.ParkingService;

@Component(value = "ParkingPTImpl")
public class ParkingPTImpl implements ParkingPT {

	private static Logger logger = LoggerFactory.getLogger(ParkingPTImpl.class);
	
	@Autowired
	private ParkingService service;

	@Override
	public ParkingResponse getParkingInformation(ParkingRequest params) {
		
		logger.info("CALLED getParkingInformation ON PARKING");
		
		try{
			ParkingResponse response = service.getParking(params);
			return response;
		}
		catch(Exception ex){
			throw new RuntimeException(ex.getMessage());
		}
	}
}
