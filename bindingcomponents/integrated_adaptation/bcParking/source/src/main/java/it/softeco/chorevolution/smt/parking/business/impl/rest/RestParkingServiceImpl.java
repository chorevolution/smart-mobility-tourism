package it.softeco.chorevolution.smt.parking.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.parking.ParkingRequest;
import it.softeco.chorevolution.smt.parking.ParkingResponse;
import it.softeco.chorevolution.smt.parking.ParkingType;
import it.softeco.chorevolution.smt.parking.business.BusinessException;
import it.softeco.chorevolution.smt.parking.business.ParkingService;

@Service
public class RestParkingServiceImpl implements ParkingService {
	
	private static Logger logger = LoggerFactory.getLogger(RestParkingServiceImpl.class);
	
	@Value("#{cfgproperties.url}")
	private String url;

	@Value("#{cfgproperties.methodget}")
	private String methodget;

	@Override
	public ParkingResponse getParking(ParkingRequest params) throws BusinessException {
		
		try{
			//build webservice url
			String baseUrl = url + methodget + "?" + "llLat=" + params.getLowerLeftLat() + "&llLon=" +params.getLowerLeftLon() + "&urLat=" +params.getUpperRightLat() +
					"&urLon=" +params.getUpperRightLon();
			
			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();
			
			switch (status) {
				case 200:
				case 201:
					
					BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
					StringBuilder sb = new StringBuilder();
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line + "\n");
					}
					br.close();
	
					if (c != null) {
						c.disconnect();
					}
					
					ParkingResponse resp = new ParkingResponse();
					// pick objs array from json
					JSONArray objs = new JSONObject(sb.toString()).getJSONArray("objs");
					for(int i=0; i<objs.length(); i++){
						ParkingType parking = new ParkingType();
						parking.setName(objs.getJSONObject(i).getString("n"));
						parking.setOperator(objs.getJSONObject(i).getString("op"));
						parking.setLat(objs.getJSONObject(i).getDouble("y"));
						parking.setLon(objs.getJSONObject(i).getDouble("x"));
						parking.setCapacity(objs.getJSONObject(i).getInt("c"));
						parking.setTariffs(objs.getJSONObject(i).getString("ta"));
						resp.getParkings().add(parking);
					}
					
					return resp;
	
				default:
					if (c != null) {
						c.disconnect();
					}
					throw new Exception("Http Error: " + status);
			}
		}
		catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
