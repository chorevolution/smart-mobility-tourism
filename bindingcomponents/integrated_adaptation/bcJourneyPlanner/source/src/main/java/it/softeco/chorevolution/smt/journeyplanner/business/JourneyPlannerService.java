package it.softeco.chorevolution.smt.journeyplanner.business;

import it.softeco.chorevolution.smt.journeyplanner.TripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.TripsResponse;

public interface JourneyPlannerService {

	TripsResponse getTrips (TripsRequest params) throws BusinessException;
}
