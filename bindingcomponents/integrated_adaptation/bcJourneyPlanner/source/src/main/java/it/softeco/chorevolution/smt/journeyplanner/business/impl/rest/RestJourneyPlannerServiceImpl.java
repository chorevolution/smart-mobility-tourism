package it.softeco.chorevolution.smt.journeyplanner.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.journeyplanner.Modes;
import it.softeco.chorevolution.smt.journeyplanner.SegmentType;
import it.softeco.chorevolution.smt.journeyplanner.TripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.TripsResponse;
import it.softeco.chorevolution.smt.journeyplanner.TripsType;
import it.softeco.chorevolution.smt.journeyplanner.business.BusinessException;
import it.softeco.chorevolution.smt.journeyplanner.business.JourneyPlannerService;

@Service
public class RestJourneyPlannerServiceImpl implements JourneyPlannerService {

	private static Logger logger = LoggerFactory.getLogger(RestJourneyPlannerServiceImpl.class);

	@Value("#{cfgproperties.url}")
	private String url;

	@Value("#{cfgproperties.methodget}")
	private String methodget;

	@Override
	public TripsResponse getTrips(TripsRequest params) throws BusinessException {

		try {
			// build webservice URL
			int transport = 0;
			switch (params.getTransportMode().value()) {
			case "Walk":
				transport = 1;
				break;
				
			case "Bicycle":
				transport = 2;
				break;
				
			case "PublicTransport":
				transport = 4;
				break;
				
			case "Car":
				transport = 8;
				break;
			}
			
			String baseUrl = url + methodget + "?" + "fromLat=" + params.getFromLat() + "&fromLon="
					+ params.getFromLon() + "&toLat=" + params.getToLat() + "&toLon=" + params.getToLon() + "&modes="
					+ transport;

			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();

			switch (status) {
			case 200:
			case 201:

				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}

				TripsResponse resp = new TripsResponse();
				JSONObject obj = new JSONObject(sb.toString());
				if(!obj.isNull("f")){
					if(!obj.getJSONObject("f").isNull("n"))
						resp.setFrom(obj.getJSONObject("f").getString("n"));
				}
				if(!obj.isNull("t")){
					if(!obj.getJSONObject("t").isNull("n"))
						resp.setTo(obj.getJSONObject("t").getString("n"));
				}
				// pick array of trips
				if(!obj.isNull("ts")){
					JSONArray ts = obj.getJSONArray("ts");
					for (int i = 0; i < ts.length(); i++) {
						TripsType t = new TripsType();
						JSONObject trip = ts.getJSONObject(i);
						t.setDistance(trip.getInt("d"));
						t.setDuration(trip.getInt("du"));
						// pick segments of the trip
						JSONArray segs = trip.getJSONArray("segs");
						for (int y = 0; y < segs.length(); y++) {
							SegmentType s = new SegmentType();
							JSONObject seg = segs.getJSONObject(y);
							s.setDistance(seg.getInt("d"));
							s.setDuration(seg.getInt("du"));
							// convert modes int to string
							int transportType = seg.getInt("t");
							switch (transportType) {
							case 1:
								s.setTransportType(Modes.WALK);
								break;
							case 4:
								s.setTransportType(Modes.CAR);
								break;
							case 8:
								s.setTransportType(Modes.BICYCLE);
								break;
							case 2:
							case 16:
							case 32:
							case 128:
								s.setTransportType(Modes.PUBLIC_TRANSPORT);
								break;
							}
							//if vehicle information, get them
							if(!seg.isNull("v")){
								s.setDropInNode(seg.getJSONObject("v").getString("s"));
								s.setDropOffNode(seg.getJSONObject("v").getString("e"));
								s.setLine(seg.getJSONObject("v").getString("l"));
								s.setVehicleDirection(seg.getJSONObject("v").getString("d"));
							}
							// pick manouevres of the segment
							if(!seg.isNull("ps")){
								JSONArray ps = seg.getJSONArray("ps");
								for (int k = 0; k < ps.length(); k++) {
										String manouevre = "";
									if(!ps.getJSONObject(k).getString("f").isEmpty())
										manouevre = "From " + ps.getJSONObject(k).getString("f");
									int manouevreCode = ps.getJSONObject(k).getInt("m");
									switch (manouevreCode) {
									case 0:
										manouevre += " Straight ";
										break;
									case 1:
										manouevre += " Right ";
										break;
									case 2:
										manouevre += " Left ";
										break;
									case 4:
										manouevre += " SlightRight ";
										break;
									case 8:
										manouevre += " SlightLeft ";
										break;
									case 16:
										manouevre += " SharpRight ";
										break;
									case 32:
										manouevre += " SharpLeft ";
										break;
									case 64:
										manouevre += " KeepLeft ";
										break;
									case 128:
										manouevre += " KeepRight ";
										break;
									}
									manouevre += " for " + ps.getJSONObject(k).getInt("d") + " meters";
									s.getManouevres().add(manouevre);
								}
							}
							t.getSegments().add(s);
						}
						resp.getTrips().add(t);
					}
				}
				return resp;

			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		} catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
