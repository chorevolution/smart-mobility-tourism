package it.softeco.chorevolution.smt.journeyplanner.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.journeyplanner.JourneyPlannerPT;
import it.softeco.chorevolution.smt.journeyplanner.TripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.TripsResponse;
import it.softeco.chorevolution.smt.journeyplanner.business.JourneyPlannerService;

@Component(value = "JourneyPlannerPTImpl")
public class JourneyPlannerPTImpl implements JourneyPlannerPT {

	private static Logger logger = LoggerFactory.getLogger(JourneyPlannerPTImpl.class);
	
	@Autowired
	private JourneyPlannerService service;

	@Override
	public TripsResponse getTripsInformation(TripsRequest params) {
		logger.info("CALLED getTripsInformation ON JOURNEYPLANNER");
		try{
			TripsResponse response = service.getTrips(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
