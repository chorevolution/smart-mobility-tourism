package it.softeco.chorevolution.smt.journeyplanner.business.impl.dummy;

import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.journeyplanner.TripsRequest;
import it.softeco.chorevolution.smt.journeyplanner.TripsResponse;
import it.softeco.chorevolution.smt.journeyplanner.business.BusinessException;
import it.softeco.chorevolution.smt.journeyplanner.business.JourneyPlannerService;

@Service
public class DummyJourneyPlannerServiceImpl implements JourneyPlannerService {

	@Override
	public TripsResponse getTrips(TripsRequest params) throws BusinessException {
		TripsResponse response = new TripsResponse();
		return response;
	}
}
