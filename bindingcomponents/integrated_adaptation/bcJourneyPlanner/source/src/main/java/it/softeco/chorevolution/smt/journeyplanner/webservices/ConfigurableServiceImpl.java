package it.softeco.chorevolution.smt.journeyplanner.webservices;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.journeyplanner.baseservice.ConfigurableService;
import it.softeco.chorevolution.smt.journeyplanner.util.SetInvocationAddressUtils;


@Component(value = "ConfigurableServiceImpl")
public class ConfigurableServiceImpl implements ConfigurableService{

	private static Logger logger = LoggerFactory.getLogger(ConfigurableServiceImpl.class);
	
	@Override
	public void setInvocationAddress(String arg0, String arg1, List<String> arg2) {
		
		logger.info("CALLED setInvocationAddress ON bcJourneyPlanner");
		SetInvocationAddressUtils.storeArtifactEndpointData(arg0, arg1, arg2);
	}

}
