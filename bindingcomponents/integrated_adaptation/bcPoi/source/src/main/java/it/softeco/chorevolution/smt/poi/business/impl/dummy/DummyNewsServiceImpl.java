package it.softeco.chorevolution.smt.poi.business.impl.dummy;

import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.poi.PoiRequest;
import it.softeco.chorevolution.smt.poi.PoiResponse;
import it.softeco.chorevolution.smt.poi.business.BusinessException;
import it.softeco.chorevolution.smt.poi.business.PoiService;

@Service
public class DummyNewsServiceImpl implements PoiService {

	@Override
	public PoiResponse getPois(PoiRequest params) throws BusinessException {
		PoiResponse response = new PoiResponse();
		return response;
	}
}
