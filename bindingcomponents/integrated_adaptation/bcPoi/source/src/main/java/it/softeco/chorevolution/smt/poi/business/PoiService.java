package it.softeco.chorevolution.smt.poi.business;

import it.softeco.chorevolution.smt.poi.PoiRequest;
import it.softeco.chorevolution.smt.poi.PoiResponse;

public interface PoiService {

	PoiResponse getPois (PoiRequest params) throws BusinessException;
}
