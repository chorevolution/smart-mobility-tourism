package it.softeco.chorevolution.smt.poi.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.poi.PoiPT;
import it.softeco.chorevolution.smt.poi.PoiRequest;
import it.softeco.chorevolution.smt.poi.PoiResponse;
import it.softeco.chorevolution.smt.poi.business.PoiService;

@Component(value = "PoiPTImpl")
public class PoiPTImpl implements PoiPT {

	private static Logger logger = LoggerFactory.getLogger(PoiPTImpl.class);
	
	@Autowired
	private PoiService service;

	@Override
	public PoiResponse getPoiList(PoiRequest params) {
		logger.info("CALLED getPoiList ON POI");
		
		try {
			PoiResponse response = service.getPois(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
