package it.softeco.chorevolution.smt.personalweatherstations.business;

import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsRequest;
import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsResponse;

public interface PersonalWeatherStationsService {

	WeatherItemsResponse getWeather(WeatherItemsRequest params) throws BusinessException;
}
