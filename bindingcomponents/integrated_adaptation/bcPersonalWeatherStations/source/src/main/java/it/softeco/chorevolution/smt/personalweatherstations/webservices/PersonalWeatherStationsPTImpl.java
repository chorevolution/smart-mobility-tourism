package it.softeco.chorevolution.smt.personalweatherstations.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.personalweatherstations.PersonalWeatherStationsPT;
import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsRequest;
import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsResponse;
import it.softeco.chorevolution.smt.personalweatherstations.business.PersonalWeatherStationsService;


@Component(value = "PersonalWeatherStationsPTImpl")
public class PersonalWeatherStationsPTImpl implements PersonalWeatherStationsPT {

	private static Logger logger = LoggerFactory.getLogger(PersonalWeatherStationsPTImpl.class);
	
	@Autowired
	private PersonalWeatherStationsService service;

	@Override
	public WeatherItemsResponse getMeteorologicalInformation(WeatherItemsRequest params) {
		
		logger.info("CALLED getMeteorologicalInformation ON PERSONALWEATHERSTATIONS");
		
		try{
			WeatherItemsResponse response = service.getWeather(params);
			return response;
		}
		catch(Exception ex){
			throw new RuntimeException(ex.getMessage());
		}
	}
}
