package it.softeco.chorevolution.smt.personalweatherstations.business.impl.dummy;

import org.springframework.stereotype.Service;


import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsRequest;
import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsResponse;
import it.softeco.chorevolution.smt.personalweatherstations.business.BusinessException;
import it.softeco.chorevolution.smt.personalweatherstations.business.PersonalWeatherStationsService;

@Service
public class DummyPersonalWeatherStationsServiceImpl implements PersonalWeatherStationsService {

	@Override
	public WeatherItemsResponse getWeather(WeatherItemsRequest params) throws BusinessException {
		WeatherItemsResponse response = new WeatherItemsResponse();
		return response;
	}
}
