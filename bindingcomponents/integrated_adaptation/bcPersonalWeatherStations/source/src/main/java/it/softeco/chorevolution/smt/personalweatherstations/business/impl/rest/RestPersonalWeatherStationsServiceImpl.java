package it.softeco.chorevolution.smt.personalweatherstations.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsRequest;
import it.softeco.chorevolution.smt.personalweatherstations.WeatherItemsResponse;
import it.softeco.chorevolution.smt.personalweatherstations.WeatherType;
import it.softeco.chorevolution.smt.personalweatherstations.business.BusinessException;
import it.softeco.chorevolution.smt.personalweatherstations.business.PersonalWeatherStationsService;

@Service
public class RestPersonalWeatherStationsServiceImpl implements PersonalWeatherStationsService {
	
private static Logger logger = LoggerFactory.getLogger(RestPersonalWeatherStationsServiceImpl.class);
	
	@Value("#{cfgproperties.url}")
	private String url;

	@Value("#{cfgproperties.methodget}")
	private String methodget;
	
	@Value("#{cfgproperties.pwd}")
	private String pwd;

	@Override
	public WeatherItemsResponse getWeather(WeatherItemsRequest params) throws BusinessException {
		
		try{
			
			//create security token
			String token = SecurityHelper.getDynamicPassword(pwd); 
			
			//build webservice url
			String baseUrl = url + methodget + "?" + "urLat=" + params.getUpperRightLat() + "&urLon=" +params.getUpperRightLon() + "&llLat=" +params.getLowerLeftLat() +
					"&llLon=" +params.getLowerLeftLon() +"&type=90" + "&s="+token;
			
			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();
			
			switch (status) {
				case 200:
				case 201:
					
					BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
					StringBuilder sb = new StringBuilder();
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line + "\n");
					}
					br.close();
	
					if (c != null) {
						c.disconnect();
					}
					
					WeatherItemsResponse resp = new WeatherItemsResponse();
					// pick objs array from json
					JSONArray objs = new JSONArray(sb.toString());
					for(int i=0; i<objs.length(); i++){
						//converto la stringa ds (contenente tutti i campi) in un oggetto JSON
						JSONObject ds = new JSONObject(objs.getJSONObject(i).getString("ds"));
						WeatherType weather = new WeatherType();
						weather.setTemperature((float) ds.getDouble("temperature"));
						weather.setHumidity(ds.getInt("humidity"));
						weather.setPressure((float) ds.getDouble("atmospheric_pressure"));
						weather.setWindSpeed((float) ds.getDouble("wind_speed"));
						weather.setRain((float) ds.getDouble("rain"));
						//prendo il jsonobj p contenente la posizione della station
						JSONObject p = objs.getJSONObject(i).getJSONObject("p");
						weather.setLat(p.getDouble("y"));
						weather.setLon(p.getDouble("x"));
						resp.getWeatherStations().add(weather);
					}
					
					
					
					return resp;
	
				default:
					if (c != null) {
						c.disconnect();
					}
					throw new Exception("Http Error: " + status);
			}
		}
		catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
