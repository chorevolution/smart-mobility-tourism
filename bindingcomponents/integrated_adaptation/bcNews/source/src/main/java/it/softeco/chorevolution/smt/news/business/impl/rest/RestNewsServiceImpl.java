package it.softeco.chorevolution.smt.news.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.news.LatestNewsRequest;
import it.softeco.chorevolution.smt.news.LatestNewsResponse;
import it.softeco.chorevolution.smt.news.NewsType;
import it.softeco.chorevolution.smt.news.business.BusinessException;
import it.softeco.chorevolution.smt.news.business.NewsService;

@Service
public class RestNewsServiceImpl implements NewsService {
	
	private static Logger logger = LoggerFactory.getLogger(RestNewsServiceImpl.class);
	
	@Value("#{cfgproperties.url}")
	private String url;

	@Value("#{cfgproperties.methodget}")
	private String methodget;

	@Override
	public LatestNewsResponse getNews(LatestNewsRequest params) throws BusinessException {
		
		try{
			//build webservice url
			String baseUrl = url + methodget + "?" + "lat=" + params.getLat() + "&lon=" +params.getLon();
			
			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();
			
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}
				
				LatestNewsResponse resp = new LatestNewsResponse();
				// pick objs array from json
				JSONArray objs = new JSONObject(sb.toString()).getJSONArray("objs");
				for(int i=0; i<objs.length(); i++){
					NewsType news = new NewsType();
					news.setName(objs.getJSONObject(i).getString("t"));
					news.setDescription(objs.getJSONObject(i).getString("c"));
					//start date
					String temp = objs.getJSONObject(i).getString("start").replace("/Date(", "").replace(")/", "");
					String[] parts = temp.split("\\+");
					long millisecs = Long.parseLong(parts[0]);
					Timestamp t = new Timestamp(millisecs);
					Date startDate = new Date(t.getTime());
					//end date
					temp = objs.getJSONObject(i).getString("end").replace("/Date(", "").replace(")/", "");
					parts = temp.split("\\+");
					millisecs = Long.parseLong(parts[0]);
					t = new Timestamp(millisecs);
					Date endDate = new Date(t.getTime());
					
					news.setStartDate(startDate);
					news.setEndDate(endDate);
					
					resp.getEvents().add(news);
				}
					
				return resp;

			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
			}
		}
		catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
