package it.softeco.chorevolution.smt.news.webservices;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.news.baseservice.ConfigurableService;
import it.softeco.chorevolution.smt.news.util.SetInvocationAddressUtils;

@Component(value = "ConfigurableServiceImpl")
public class ConfigurableServiceImpl implements ConfigurableService {

	private static Logger logger = LoggerFactory.getLogger(ConfigurableServiceImpl.class);

	@Override
	public void setInvocationAddress(String arg0, String arg1, List<String> arg2) {

		logger.info("CALLED setInvocationAddress ON bcNews");
		SetInvocationAddressUtils.storeArtifactEndpointData(arg0, arg1, arg2);
	}

}
