package it.softeco.chorevolution.smt.news.business.impl.dummy;

import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.news.LatestNewsRequest;
import it.softeco.chorevolution.smt.news.LatestNewsResponse;
import it.softeco.chorevolution.smt.news.business.BusinessException;
import it.softeco.chorevolution.smt.news.business.NewsService;

@Service
public class DummyNewsServiceImpl implements NewsService {
	
	@Override
	public LatestNewsResponse getNews(LatestNewsRequest params) throws BusinessException {
		LatestNewsResponse response = new LatestNewsResponse();
		return response;
	}
}
