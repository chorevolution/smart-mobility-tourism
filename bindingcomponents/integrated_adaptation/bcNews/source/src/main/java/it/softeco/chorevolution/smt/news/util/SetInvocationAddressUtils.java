package it.softeco.chorevolution.smt.news.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SetInvocationAddressUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SetInvocationAddressUtils.class);

	// role, ArtifactAddressData
	private static Map<String, ArtifactEndpointData> artifactsEndpointsData = new HashMap<>();
	
	public static void storeArtifactEndpointData(String role, String name, List<String> endpoints){
		
		artifactsEndpointsData.put(role, new ArtifactEndpointData(name, role, endpoints));
		LOGGER.info("BC NEWS set invocation address for artifact " + name + " with role " + role + " to " + endpoints.get(0));
	}
	
	public static String getArtifactEndpointAddressFromRole(String role) {

		if (artifactsEndpointsData.containsKey(role)) {
			String address = artifactsEndpointsData.get(role).getEndpoints().get(0);
			LOGGER.info("BC NEWS found invocation address to call artifact with role " + role + ": " + address);
			return address;
		} else {
			LOGGER.info("BC NEWS has NOT found invocation address to call artifact with role " + role);
			return null;
		}
	}
}
