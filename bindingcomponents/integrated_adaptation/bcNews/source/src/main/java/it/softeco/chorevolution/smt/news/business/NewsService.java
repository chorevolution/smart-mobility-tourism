package it.softeco.chorevolution.smt.news.business;

import it.softeco.chorevolution.smt.news.LatestNewsRequest;
import it.softeco.chorevolution.smt.news.LatestNewsResponse;

public interface NewsService {

	LatestNewsResponse getNews(LatestNewsRequest params) throws BusinessException;
}
