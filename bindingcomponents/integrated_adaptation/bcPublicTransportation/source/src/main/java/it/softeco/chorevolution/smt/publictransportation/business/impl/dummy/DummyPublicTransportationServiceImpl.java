package it.softeco.chorevolution.smt.publictransportation.business.impl.dummy;

import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.publictransportation.LatestPTRequest;
import it.softeco.chorevolution.smt.publictransportation.LatestPTResponse;
import it.softeco.chorevolution.smt.publictransportation.business.BusinessException;
import it.softeco.chorevolution.smt.publictransportation.business.PublicTransportationService;

@Service
public class DummyPublicTransportationServiceImpl implements PublicTransportationService {

	@Override
	public LatestPTResponse getPTInfo(LatestPTRequest params) throws BusinessException {
		LatestPTResponse response = new LatestPTResponse();
		return response;
	}
}
