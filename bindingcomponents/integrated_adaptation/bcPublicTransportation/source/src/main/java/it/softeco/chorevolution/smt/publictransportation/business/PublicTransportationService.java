package it.softeco.chorevolution.smt.publictransportation.business;

import it.softeco.chorevolution.smt.publictransportation.LatestPTRequest;
import it.softeco.chorevolution.smt.publictransportation.LatestPTResponse;

public interface PublicTransportationService {

	LatestPTResponse getPTInfo (LatestPTRequest params) throws BusinessException;
}
