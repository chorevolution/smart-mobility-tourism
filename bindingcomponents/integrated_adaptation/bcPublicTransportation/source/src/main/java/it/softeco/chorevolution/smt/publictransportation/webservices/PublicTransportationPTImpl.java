package it.softeco.chorevolution.smt.publictransportation.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.publictransportation.LatestPTRequest;
import it.softeco.chorevolution.smt.publictransportation.LatestPTResponse;
import it.softeco.chorevolution.smt.publictransportation.PublicTransportationPT;
import it.softeco.chorevolution.smt.publictransportation.business.PublicTransportationService;

@Component(value = "PublicTransportationPTImpl")
public class PublicTransportationPTImpl implements PublicTransportationPT {

	private static Logger logger = LoggerFactory.getLogger(PublicTransportationPTImpl.class);
	
	@Autowired
	private PublicTransportationService service;

	@Override
	public LatestPTResponse getPublicTransportationInfo(LatestPTRequest params) {
		logger.info("CALLED getPublicTransportationInfo ON PUBLICTRANSPORTATION");
		
		try{
			LatestPTResponse response = service.getPTInfo(params);
			return response;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
}
