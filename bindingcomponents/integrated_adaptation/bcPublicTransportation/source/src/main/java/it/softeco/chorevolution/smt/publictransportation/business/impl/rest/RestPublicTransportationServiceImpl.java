package it.softeco.chorevolution.smt.publictransportation.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.publictransportation.LatestPTRequest;
import it.softeco.chorevolution.smt.publictransportation.LatestPTResponse;
import it.softeco.chorevolution.smt.publictransportation.NewsType;
import it.softeco.chorevolution.smt.publictransportation.business.BusinessException;
import it.softeco.chorevolution.smt.publictransportation.business.PublicTransportationService;

@Service
public class RestPublicTransportationServiceImpl implements PublicTransportationService {

	private static Logger logger = LoggerFactory.getLogger(RestPublicTransportationServiceImpl.class);
	
	@Value("#{cfgproperties.url}")
	private String url;

	@Value("#{cfgproperties.methodget}")
	private String methodget;
	
	@Override
	public LatestPTResponse getPTInfo(LatestPTRequest params) throws BusinessException {
		
		try{
			//build webservice url
			String baseUrl = url + methodget + "?" + "lat=44.47" + "&lon=9.08";
			
			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();
			
			switch (status) {
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}
				
				LatestPTResponse response = new LatestPTResponse();
				// pick news from json
				JSONArray objs = new JSONArray(sb.toString()); 
				for(int i=0; i<objs.length(); i++){
					
					NewsType news = new NewsType();
					news.setName(objs.getJSONObject(i).getString("t"));
					news.setDescription(objs.getJSONObject(i).getString("c"));
					
					//start date
					String temp = objs.getJSONObject(i).getString("start").replace("/Date(", "").replace(")/", "");
					String[] parts = temp.split("\\+");
					long millisecs = Long.parseLong(parts[0]);
					Timestamp t = new Timestamp(millisecs);
					Date startDate = new Date(t.getTime());
					news.setStartDate(startDate);
					
					//end date
					if(!objs.getJSONObject(i).isNull("end")){
						temp = objs.getJSONObject(i).getString("end").replace("/Date(", "").replace(")/", "");
						parts = temp.split("\\+");
						millisecs = Long.parseLong(parts[0]);
						t = new Timestamp(millisecs);
						Date endDate = new Date(t.getTime());
						news.setEndDate(endDate);
					}
					
					response.getPublicTransportNews().add(news);
				}
					
				return response;

				default:
					if (c != null) {
						c.disconnect();
					}
					throw new Exception("Http Error: " + status);
			}
		}
		catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
