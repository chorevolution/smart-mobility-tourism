package it.softeco.chorevolution.smt.traffic.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.softeco.chorevolution.smt.traffic.TrafficPT;
import it.softeco.chorevolution.smt.traffic.TrafficRequest;
import it.softeco.chorevolution.smt.traffic.TrafficResponse;
import it.softeco.chorevolution.smt.traffic.business.TrafficService;

@Component(value = "TrafficPTImpl")
public class TrafficPTImpl implements TrafficPT {

	private static Logger logger = LoggerFactory.getLogger(TrafficPTImpl.class);
	
	@Autowired
	private TrafficService service;

	@Override
	public TrafficResponse getTrafficInformation(TrafficRequest params) {
		
		logger.info("CALLED getTrafficInformation ON TRAFFIC");
		
		try{
			TrafficResponse response = service.getTraffic(params);
			return response;
		}
		catch(Exception ex){
			throw new RuntimeException(ex.getMessage());
		}
	}
}
