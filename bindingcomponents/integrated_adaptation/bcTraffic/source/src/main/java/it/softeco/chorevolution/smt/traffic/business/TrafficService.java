package it.softeco.chorevolution.smt.traffic.business;

import it.softeco.chorevolution.smt.traffic.TrafficRequest;
import it.softeco.chorevolution.smt.traffic.TrafficResponse;

public interface TrafficService {

	TrafficResponse getTraffic (TrafficRequest params) throws BusinessException;
}
