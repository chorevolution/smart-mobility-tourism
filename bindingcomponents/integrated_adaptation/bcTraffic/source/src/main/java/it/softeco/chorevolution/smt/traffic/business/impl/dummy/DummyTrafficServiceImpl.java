package it.softeco.chorevolution.smt.traffic.business.impl.dummy;

import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.traffic.TrafficRequest;
import it.softeco.chorevolution.smt.traffic.TrafficResponse;
import it.softeco.chorevolution.smt.traffic.business.BusinessException;
import it.softeco.chorevolution.smt.traffic.business.TrafficService;

@Service
public class DummyTrafficServiceImpl implements TrafficService {

	@Override
	public TrafficResponse getTraffic(TrafficRequest params) throws BusinessException {
		TrafficResponse response = new TrafficResponse();
		return response;
	}
}
