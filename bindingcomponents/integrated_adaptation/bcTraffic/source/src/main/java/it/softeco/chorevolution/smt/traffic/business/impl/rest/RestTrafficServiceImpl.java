package it.softeco.chorevolution.smt.traffic.business.impl.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.softeco.chorevolution.smt.traffic.TrafficRequest;
import it.softeco.chorevolution.smt.traffic.TrafficResponse;
import it.softeco.chorevolution.smt.traffic.TrafficType;
import it.softeco.chorevolution.smt.traffic.business.BusinessException;
import it.softeco.chorevolution.smt.traffic.business.TrafficService;

@Service
public class RestTrafficServiceImpl implements TrafficService {
	
	private static Logger logger = LoggerFactory.getLogger(RestTrafficServiceImpl.class);
	
	@Value("#{cfgproperties.url}")
	private String url;

	@Value("#{cfgproperties.methodget}")
	private String methodget;

	@Override
	public TrafficResponse getTraffic(TrafficRequest params) throws BusinessException {
		
		try
		{
			//build webservice url
			String baseUrl = url + methodget + "?" + "urLat=" + params.getUpperRightLat() + "&urLon=" + params.getUpperRightLon() + "&llLat=" + params.getLowerLeftLat() +
					"&llLon=" + params.getLowerLeftLon();
			
			// connect to url
			URL u = new URL(baseUrl);
			HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Content-length", "0");
			c.setUseCaches(false);
			c.setAllowUserInteraction(false);
			c.connect();
			int status = c.getResponseCode();
			
			switch(status)
			{
			case 200:
			case 201:
				BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();

				if (c != null) {
					c.disconnect();
				}
				
				TrafficResponse response = new TrafficResponse();
				//pick traffic events from json
				JSONArray objs = new JSONArray(sb.toString());
				for(int i=0; i<objs.length(); i++){
				
					TrafficType traffic = new TrafficType();
					traffic.setDescription(objs.getJSONObject(i).getString("desc"));
					traffic.setLat(objs.getJSONObject(i).getDouble("y"));
					traffic.setLon(objs.getJSONObject(i).getDouble("x"));
					
					//start date
					String temp = objs.getJSONObject(i).getString("s").replace("/Date(", "").replace(")/", "");
					String[] parts = temp.split("\\+");
					long millisecs = Long.parseLong(parts[0]);
					Timestamp t = new Timestamp(millisecs);
					Date startDate = new Date(t.getTime());
					traffic.setStartDate(startDate);
					
					//end date
					if(!objs.getJSONObject(i).isNull("end")){
						temp = objs.getJSONObject(i).getString("e").replace("/Date(", "").replace(")/", "");
						parts = temp.split("\\+");
						millisecs = Long.parseLong(parts[0]);
						t = new Timestamp(millisecs);
						Date endDate = new Date(t.getTime());
						traffic.setEndDate(endDate);
					}
					response.getTrafficInfos().add(traffic);
				}
				
				return response;
				
			default:
				if (c != null) {
					c.disconnect();
				}
				throw new Exception("Http Error: " + status);
				
				
			}
		}
		catch (Exception e) {
			logger.error("error", e);
			throw new BusinessException(e);
		}
	}
}
