package eu.chorevolution.ad.parking.integration.util;

import java.util.Map;

import org.w3c.dom.NodeList;

public class TransformationRuleHandler {

	public static String handleRule(String rule, NodeList nodes, Map<String, Integer> occurrencesMap) {
		String output = "";
		String[] ruleElements = rule.split("\\+");

		for (int i=0; i<ruleElements.length; i++) {
			String ruleElement = ruleElements[i].trim();

			if (ruleElement.matches("\"([^\"]*)\"")) {
				output = output + ruleElement.substring(1, ruleElement.length()-1);

			} else if (ruleElement.matches("valueof\\(([^\\)]*)\\)")) {
				output = output + transformValueof(ruleElement, nodes, occurrencesMap);

			} else {
				output = output + TransformationUtils.findNodeValue(nodes, ruleElement, occurrencesMap);
				
			}
		}
		return output;
	}


	private static String transformValueof(String rule, NodeList nodes, Map<String, Integer> occurrencesMap) {

		String[] ruleElements = rule.substring(8, rule.length()-1).split(",");

		if (ruleElements.length>1) {
			String translatingElement = ruleElements[0].trim();

			String elementValue = TransformationUtils.findNodeValue(nodes, translatingElement, occurrencesMap);

			for (int i=1; i<ruleElements.length; i++) {
				String association = ruleElements[i].trim();
				String[] valueMapping = association.split(":");
				if(valueMapping.length==2) {
					if (valueMapping[0].equals(elementValue)) {
						return valueMapping[1];
					}
				}
			}
		}
		return "";
	}
}
