package eu.chorevolution.ad.parking.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.parking.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.parking.integration.util.TransformationUtils;

@Component
public class ResponseTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseTranslator.class);

	public Message<String> translate(String input) {

		Document inputDoc = TransformationUtils.convertStringToDocument(input);
		NodeList nodes = inputDoc.getFirstChild().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:parkingResponse xmlns:ns=\"http://eu.chorevolution.smt/ad/adParking\">";

		int outputDataTypeobjsNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType.objs", occurrencesMap);
		int outputDataTypeobjsCounter = 1;
		while (outputDataTypeobjsCounter <= outputDataTypeobjsNumber) {
			occurrencesMap.put("outputDataType.objs", outputDataTypeobjsCounter);
		output = output + "<parkings>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.n", occurrencesMap).isEmpty()) {
			output = output + "<name>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.n", occurrencesMap)) + "</name>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.op", occurrencesMap).isEmpty()) {
			output = output + "<operator>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.op", occurrencesMap)) + "</operator>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.y", occurrencesMap).isEmpty()) {
			output = output + "<lat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.y", occurrencesMap)) + "</lat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.x", occurrencesMap).isEmpty()) {
			output = output + "<lon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.x", occurrencesMap)) + "</lon>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.c", occurrencesMap).isEmpty()) {
			output = output + "<capacity>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.c", occurrencesMap)) + "</capacity>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.ta", occurrencesMap).isEmpty()) {
			output = output + "<tariffs>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.ta", occurrencesMap)) + "</tariffs>";
		}
		output = output + "</parkings>";
					outputDataTypeobjsCounter++;
		}


		output = output + "</ns:parkingResponse>";

		return MessageBuilder.withPayload(output).build();
	}
}
