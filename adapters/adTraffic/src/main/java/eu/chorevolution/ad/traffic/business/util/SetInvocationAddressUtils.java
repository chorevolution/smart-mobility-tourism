package eu.chorevolution.ad.traffic.business.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.ad.traffic.business.model.ArtifactEndpointData;

public class SetInvocationAddressUtils {

	private static final String DEFAULT_ADDRESS = "http://localhost:9090/bcTraffic/bcTraffic";

	private static final Logger LOGGER = LoggerFactory.getLogger(SetInvocationAddressUtils.class);

	// role, ArtifactAddressData
	private static Map<String, ArtifactEndpointData> artifactsEndpointsData = new HashMap<>();

	public static void storeArtifactEndpointData(String role, String name, List<String> endpoints) {

		artifactsEndpointsData.put(role, new ArtifactEndpointData(name, role, endpoints));
		LOGGER.info("Adapter adTraffic set invocation address for artifact " + name + " with role " + role + " to " + endpoints.get(0));
	}

	public static String getArtifactEndpointAddressFromRole(String role) {

		if (artifactsEndpointsData.containsKey(role)) {
			String address = artifactsEndpointsData.get(role).getEndpoints().get(0);
			LOGGER.info(
					"Adapter adTraffic found invocation address to call artifact with role " + role + ": " + address);
			return address;
		} else {
			LOGGER.info("Adapter adTraffic has NOT found invocation address to call artifact with role " + role);
			return null;
		}
	}

	public static String getTargetEndpointAddress() {
		if (!artifactsEndpointsData.values().isEmpty()) {
			String address = artifactsEndpointsData.values().iterator().next().getEndpoints().get(0);
			LOGGER.info("Adapter adTraffic found invocation addres to call target artifact: " + address);
			return address;
		} else {
			LOGGER.info("Adapter adTraffic is using default address to call target artifact: " + DEFAULT_ADDRESS);
			return DEFAULT_ADDRESS;
		}
	}
}
