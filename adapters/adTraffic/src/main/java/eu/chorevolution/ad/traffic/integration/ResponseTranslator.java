package eu.chorevolution.ad.traffic.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.traffic.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.traffic.integration.util.TransformationUtils;

@Component
public class ResponseTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseTranslator.class);

	public Message<String> translate(String input) {

		Document inputDoc = TransformationUtils.convertStringToDocument(input);
		NodeList nodes = inputDoc.getFirstChild().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:trafficResponse xmlns:ns=\"http://eu.chorevolution.smt/ad/adTraffic\">";

		int outputDataTypeNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType", occurrencesMap);
		int outputDataTypeCounter = 1;
		while (outputDataTypeCounter <= outputDataTypeNumber) {
			occurrencesMap.put("outputDataType", outputDataTypeCounter);
		output = output + "<trafficInfos>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.n", occurrencesMap).isEmpty()) {
			output = output + "<name>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.n", occurrencesMap)) + "</name>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.y", occurrencesMap).isEmpty()) {
			output = output + "<lat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.y", occurrencesMap)) + "</lat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.x", occurrencesMap).isEmpty()) {
			output = output + "<lon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.x", occurrencesMap)) + "</lon>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.desc", occurrencesMap).isEmpty()) {
			output = output + "<description>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.desc", occurrencesMap)) + "</description>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.s", occurrencesMap).isEmpty()) {
			output = output + "<startDate>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.s", occurrencesMap)) + "</startDate>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.e", occurrencesMap).isEmpty()) {
			output = output + "<endDate>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.e", occurrencesMap)) + "</endDate>";
		}
		output = output + "</trafficInfos>";
					outputDataTypeCounter++;
		}


		output = output + "</ns:trafficResponse>";

		return MessageBuilder.withPayload(output).build();
	}
}
