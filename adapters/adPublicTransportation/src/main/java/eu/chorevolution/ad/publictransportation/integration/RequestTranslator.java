package eu.chorevolution.ad.publictransportation.integration;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.dom.DOMSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.publictransportation.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.publictransportation.integration.util.TransformationUtils;


@Component
public class RequestTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestTranslator.class);


	public Message<String> translate(DOMSource input) {		
		NodeList nodes = input.getNode().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:publicTransportation xmlns:ns=\"http://eu.chorevolution.smt/bc/bcPublicTransportation\">";

		output = output + "<inputDataType>";
		String inputDataTypelatOutValue = TransformationRuleHandler.handleRule(StringEscapeUtils.unescapeXml("&quot;44.47&quot;"), nodes, occurrencesMap);
		output = output + "<lat>" + StringEscapeUtils.escapeXml(inputDataTypelatOutValue) + "</lat>";
		String inputDataTypelonOutValue = TransformationRuleHandler.handleRule(StringEscapeUtils.unescapeXml("&quot;9.08&quot;"), nodes, occurrencesMap);
		output = output + "<lon>" + StringEscapeUtils.escapeXml(inputDataTypelonOutValue) + "</lon>";
		output = output + "</inputDataType>";


		output = output + "</ns:publicTransportation>";

		return MessageBuilder.withPayload(output).build();
	}
}
