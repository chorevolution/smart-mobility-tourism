package eu.chorevolution.ad.publictransportation.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.publictransportation.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.publictransportation.integration.util.TransformationUtils;

@Component
public class ResponseTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseTranslator.class);

	public Message<String> translate(String input) {

		Document inputDoc = TransformationUtils.convertStringToDocument(input);
		NodeList nodes = inputDoc.getFirstChild().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:latestPTResponse xmlns:ns=\"http://eu.chorevolution.smt/ad/adPublicTransportation\">";

		int outputDataTypeNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType", occurrencesMap);
		int outputDataTypeCounter = 1;
		while (outputDataTypeCounter <= outputDataTypeNumber) {
			occurrencesMap.put("outputDataType", outputDataTypeCounter);
		output = output + "<publicTransportNews>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.t", occurrencesMap).isEmpty()) {
			output = output + "<name>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.t", occurrencesMap)) + "</name>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.c", occurrencesMap).isEmpty()) {
			output = output + "<description>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.c", occurrencesMap)) + "</description>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.start", occurrencesMap).isEmpty()) {
			output = output + "<startDate>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.start", occurrencesMap)) + "</startDate>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.start", occurrencesMap).isEmpty()) {
			output = output + "<endDate>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.start", occurrencesMap)) + "</endDate>";
		}
		output = output + "</publicTransportNews>";
					outputDataTypeCounter++;
		}


		output = output + "</ns:latestPTResponse>";

		return MessageBuilder.withPayload(output).build();
	}
}
