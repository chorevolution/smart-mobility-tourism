package eu.chorevolution.ad.journeyplanner.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.journeyplanner.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.journeyplanner.integration.util.TransformationUtils;

@Component
public class ResponseTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseTranslator.class);

	public Message<String> translate(String input) {

		Document inputDoc = TransformationUtils.convertStringToDocument(input);
		NodeList nodes = inputDoc.getFirstChild().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:tripsResponse xmlns:ns=\"http://eu.chorevolution.smt/ad/adJourneyPlanner\">";

		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.f.n", occurrencesMap).isEmpty()) {
			output = output + "<from>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.f.n", occurrencesMap)) + "</from>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.t.n", occurrencesMap).isEmpty()) {
			output = output + "<to>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.t.n", occurrencesMap)) + "</to>";
		}
		int outputDataTypetsNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType.ts", occurrencesMap);
		int outputDataTypetsCounter = 1;
		while (outputDataTypetsCounter <= outputDataTypetsNumber) {
			occurrencesMap.put("outputDataType.ts", outputDataTypetsCounter);
		output = output + "<trips>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.d", occurrencesMap).isEmpty()) {
			output = output + "<distance>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.d", occurrencesMap)) + "</distance>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.du", occurrencesMap).isEmpty()) {
			output = output + "<duration>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.du", occurrencesMap)) + "</duration>";
		}
		int outputDataTypetssegsNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType.ts.segs", occurrencesMap);
		int outputDataTypetssegsCounter = 1;
		while (outputDataTypetssegsCounter <= outputDataTypetssegsNumber) {
			occurrencesMap.put("outputDataType.ts.segs", outputDataTypetssegsCounter);
		output = output + "<segments>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.d", occurrencesMap).isEmpty()) {
			output = output + "<distance>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.d", occurrencesMap)) + "</distance>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.du", occurrencesMap).isEmpty()) {
			output = output + "<duration>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.du", occurrencesMap)) + "</duration>";
		}
		String tripssegmentstransportTypeOutValue = TransformationRuleHandler.handleRule(StringEscapeUtils.unescapeXml("valueof(outputDataType.ts.segs.t, 1:Walk, 4:Car, 8:Bicycle, 2:PublicTransport, 16:PublicTransport, 32:PublicTransport, 128:PublicTransport)"), nodes, occurrencesMap);
		output = output + "<transportType>" + StringEscapeUtils.escapeXml(tripssegmentstransportTypeOutValue) + "</transportType>"; 
		int outputDataTypetssegspsNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType.ts.segs.ps", occurrencesMap);
		int outputDataTypetssegspsCounter = 1;
		while (outputDataTypetssegspsCounter <= outputDataTypetssegspsNumber) {
			occurrencesMap.put("outputDataType.ts.segs.ps", outputDataTypetssegspsCounter);
		String tripssegmentsmanouevresOutValue = TransformationRuleHandler.handleRule(StringEscapeUtils.unescapeXml("&quot;From &quot; + outputDataType.ts.segs.ps.f + &quot; &quot; + valueof(outputDataType.ts.segs.ps.m, 0:Straight, 1:Right, 2:Left, 4:Slight Right, 5:Slight Left, 16:Sharp Right, 32:Sharp Left, 64:Keep Left, 128:Keep Right) + &quot; for &quot; + outputDataType.ts.segs.ps.d + &quot; meters&quot;"), nodes, occurrencesMap);
		output = output + "<manouevres>" + StringEscapeUtils.escapeXml(tripssegmentsmanouevresOutValue) + "</manouevres>"; 
		 
			outputDataTypetssegspsCounter++;
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.s", occurrencesMap).isEmpty()) {
			output = output + "<dropInNode>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.s", occurrencesMap)) + "</dropInNode>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.e", occurrencesMap).isEmpty()) {
			output = output + "<dropOffNode>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.e", occurrencesMap)) + "</dropOffNode>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.l", occurrencesMap).isEmpty()) {
			output = output + "<line>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.l", occurrencesMap)) + "</line>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.d", occurrencesMap).isEmpty()) {
			output = output + "<vehicleDirection>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.ts.segs.v.d", occurrencesMap)) + "</vehicleDirection>";
		}
		output = output + "</segments>";
					outputDataTypetssegsCounter++;
		}
		output = output + "</trips>";
					outputDataTypetsCounter++;
		}


		output = output + "</ns:tripsResponse>";

		return MessageBuilder.withPayload(output).build();
	}
}
