package eu.chorevolution.ad.journeyplanner.integration;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.dom.DOMSource;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.journeyplanner.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.journeyplanner.integration.util.TransformationUtils;


@Component
public class RequestTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestTranslator.class);


	public Message<String> translate(DOMSource input) {		
		NodeList nodes = input.getNode().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:JourneyPlanningService xmlns:ns=\"http://eu.chorevolution.smt/bc/bcJourneyPlanner\">";

		output = output + "<inputDataType>";
		if(!TransformationUtils.findNodeValue(nodes, "fromLat", occurrencesMap).isEmpty()) {
			output = output + "<fromLat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "fromLat", occurrencesMap)) + "</fromLat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "fromLon", occurrencesMap).isEmpty()) {
			output = output + "<fromLon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "fromLon", occurrencesMap)) + "</fromLon>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "toLat", occurrencesMap).isEmpty()) {
			output = output + "<toLat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "toLat", occurrencesMap)) + "</toLat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "toLon", occurrencesMap).isEmpty()) {
			output = output + "<toLon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "toLon", occurrencesMap)) + "</toLon>";
		}
		String inputDataTypemodesOutValue = TransformationRuleHandler.handleRule(StringEscapeUtils.unescapeXml("valueof(transportMode, Walk:1, Bicycle:2, PublicTransport:4, Car:8)"), nodes, occurrencesMap);
		output = output + "<modes>" + StringEscapeUtils.escapeXml(inputDataTypemodesOutValue) + "</modes>";
		output = output + "</inputDataType>";


		output = output + "</ns:JourneyPlanningService>";

		return MessageBuilder.withPayload(output).build();
	}
}
