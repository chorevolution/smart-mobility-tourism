package eu.chorevolution.ad.personalweatherstations.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.personalweatherstations.integration.util.TransformationUtils;

@Component
public class ResponseTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseTranslator.class);

	public Message<String> translate(String input) {

		Document inputDoc = TransformationUtils.convertStringToDocument(input);
		NodeList nodes = inputDoc.getFirstChild().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:weatherItemsResponse xmlns:ns=\"http://eu.chorevolution.smt/ad/adPersonalWeatherStations\">";

		int outputDataTypeNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType", occurrencesMap);
		int outputDataTypeCounter = 1;
		while (outputDataTypeCounter <= outputDataTypeNumber) {
			occurrencesMap.put("outputDataType", outputDataTypeCounter);
		output = output + "<weatherStations>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap).isEmpty()) {
			JSONObject temperatureJSON = new JSONObject(TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap));
			output = output + "<temperature>" + StringEscapeUtils.escapeXml(temperatureJSON.getString("temperature")) + "</temperature>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap).isEmpty()) {
			JSONObject humidityJSON = new JSONObject(TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap));
			output = output + "<humidity>" + StringEscapeUtils.escapeXml(humidityJSON.getString("humidity")) + "</humidity>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.p.y", occurrencesMap).isEmpty()) {
			output = output + "<lat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.p.y", occurrencesMap)) + "</lat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.p.x", occurrencesMap).isEmpty()) {
			output = output + "<lon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.p.x", occurrencesMap)) + "</lon>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap).isEmpty()) {
			JSONObject pressureJSON = new JSONObject(TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap));
			output = output + "<pressure>" + StringEscapeUtils.escapeXml(pressureJSON.getString("atmospheric_pressure")) + "</pressure>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap).isEmpty()) {
			JSONObject windSpeedJSON = new JSONObject(TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap));
			output = output + "<windSpeed>" + StringEscapeUtils.escapeXml(windSpeedJSON.getString("wind_speed")) + "</windSpeed>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap).isEmpty()) {
			JSONObject rainJSON = new JSONObject(TransformationUtils.findNodeValue(nodes, "outputDataType.ds", occurrencesMap));
			output = output + "<rain>" + StringEscapeUtils.escapeXml(rainJSON.getString("rain")) + "</rain>";
		}
		output = output + "</weatherStations>";
					outputDataTypeCounter++;
		}


		output = output + "</ns:weatherItemsResponse>";

		return MessageBuilder.withPayload(output).build();
	}
}
