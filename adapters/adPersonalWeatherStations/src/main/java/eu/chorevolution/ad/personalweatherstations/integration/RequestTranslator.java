package eu.chorevolution.ad.personalweatherstations.integration;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.dom.DOMSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.personalweatherstations.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.personalweatherstations.integration.util.TransformationUtils;


@Component
public class RequestTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestTranslator.class);


	public Message<String> translate(DOMSource input) {		
		NodeList nodes = input.getNode().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:ItemService xmlns:ns=\"http://eu.chorevolution.smt/bc/bcPersonalWeatherStations\">";

		output = output + "<inputDataType>";
		if(!TransformationUtils.findNodeValue(nodes, "upperRightLat", occurrencesMap).isEmpty()) {
			output = output + "<urLat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "upperRightLat", occurrencesMap)) + "</urLat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "upperRightLon", occurrencesMap).isEmpty()) {
			output = output + "<urLon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "upperRightLon", occurrencesMap)) + "</urLon>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "lowerLeftLat", occurrencesMap).isEmpty()) {
			output = output + "<llLat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "lowerLeftLat", occurrencesMap)) + "</llLat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "lowerLeftLon", occurrencesMap).isEmpty()) {
			output = output + "<llLon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "lowerLeftLon", occurrencesMap)) + "</llLon>";
		}
		String inputDataTypetypeOutValue = TransformationRuleHandler.handleRule(StringEscapeUtils.unescapeXml("&quot;90&quot;"), nodes, occurrencesMap);
		output = output + "<type>" + StringEscapeUtils.escapeXml(inputDataTypetypeOutValue) + "</type>";
		String inputDataTypesOutValue = TransformationRuleHandler.handleRule(StringEscapeUtils.unescapeXml("&quot;s&quot;"), nodes, occurrencesMap);
		output = output + "<s>" + StringEscapeUtils.escapeXml(inputDataTypesOutValue) + "</s>";
		output = output + "</inputDataType>";


		output = output + "</ns:ItemService>";

		return MessageBuilder.withPayload(output).build();
	}
}
