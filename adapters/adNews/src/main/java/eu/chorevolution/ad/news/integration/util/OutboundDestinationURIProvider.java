package eu.chorevolution.ad.news.integration.util;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.support.destination.DestinationProvider;

import eu.chorevolution.ad.news.business.util.SetInvocationAddressUtils;

@Component
public class OutboundDestinationURIProvider implements DestinationProvider {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutboundDestinationURIProvider.class);

	@Override
	public URI getDestination() {
		try {
// 			String address = SetInvocationAddressUtils.getArtifactEndpointAddressFromRole("News");
			String address = SetInvocationAddressUtils.getTargetEndpointAddress();
			LOGGER.info("Invocation address for News: " + address);			
			return new URI(address);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}
}
