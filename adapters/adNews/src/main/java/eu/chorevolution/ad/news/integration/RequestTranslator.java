package eu.chorevolution.ad.news.integration;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.dom.DOMSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.news.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.news.integration.util.TransformationUtils;


@Component
public class RequestTranslator {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestTranslator.class);


	public Message<String> translate(DOMSource input) {		
		NodeList nodes = input.getNode().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:NewsService xmlns:ns=\"http://eu.chorevolution.smt/bc/bcNews\">";

		output = output + "<inputDataType>";
		if(!TransformationUtils.findNodeValue(nodes, "lat", occurrencesMap).isEmpty()) {
			output = output + "<lat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "lat", occurrencesMap)) + "</lat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "lon", occurrencesMap).isEmpty()) {
			output = output + "<lon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "lon", occurrencesMap)) + "</lon>";
		}
		output = output + "</inputDataType>";


		output = output + "</ns:NewsService>";

		return MessageBuilder.withPayload(output).build();
	}
}
