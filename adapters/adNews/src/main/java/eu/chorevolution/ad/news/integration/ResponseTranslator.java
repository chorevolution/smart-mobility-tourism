package eu.chorevolution.ad.news.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.news.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.news.integration.util.TransformationUtils;

@Component
public class ResponseTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseTranslator.class);

	public Message<String> translate(String input) {

		Document inputDoc = TransformationUtils.convertStringToDocument(input);
		NodeList nodes = inputDoc.getFirstChild().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:latestNewsResponse xmlns:ns=\"http://eu.chorevolution.smt/ad/adNews\">";

		int outputDataTypeobjsNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType.objs", occurrencesMap);
		int outputDataTypeobjsCounter = 1;
		while (outputDataTypeobjsCounter <= outputDataTypeobjsNumber) {
			occurrencesMap.put("outputDataType.objs", outputDataTypeobjsCounter);
		output = output + "<events>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.t", occurrencesMap).isEmpty()) {
			output = output + "<name>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.t", occurrencesMap)) + "</name>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.c", occurrencesMap).isEmpty()) {
			output = output + "<description>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.c", occurrencesMap)) + "</description>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.start", occurrencesMap).isEmpty()) {
			output = output + "<startDate>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.start", occurrencesMap)) + "</startDate>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.end", occurrencesMap).isEmpty()) {
			output = output + "<endDate>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.end", occurrencesMap)) + "</endDate>";
		}
		output = output + "</events>";
					outputDataTypeobjsCounter++;
		}


		output = output + "</ns:latestNewsResponse>";

		return MessageBuilder.withPayload(output).build();
	}
}
