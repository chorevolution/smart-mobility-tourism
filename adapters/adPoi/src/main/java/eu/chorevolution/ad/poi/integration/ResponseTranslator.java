package eu.chorevolution.ad.poi.integration;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.chorevolution.ad.poi.integration.util.TransformationRuleHandler;
import eu.chorevolution.ad.poi.integration.util.TransformationUtils;

@Component
public class ResponseTranslator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseTranslator.class);

	public Message<String> translate(String input) {

		Document inputDoc = TransformationUtils.convertStringToDocument(input);
		NodeList nodes = inputDoc.getFirstChild().getChildNodes();

		Map<String, Integer> occurrencesMap = new HashMap<>();

		String output = "<ns:poiResponse xmlns:ns=\"http://eu.chorevolution.smt/ad/adPoi\">";

		int outputDataTypeobjsNumber = TransformationUtils.findNumberOfOccurrences(nodes, "outputDataType.objs", occurrencesMap);
		int outputDataTypeobjsCounter = 1;
		while (outputDataTypeobjsCounter <= outputDataTypeobjsNumber) {
			occurrencesMap.put("outputDataType.objs", outputDataTypeobjsCounter);
		output = output + "<pois>";
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.n", occurrencesMap).isEmpty()) {
			output = output + "<name>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.n", occurrencesMap)) + "</name>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.ds", occurrencesMap).isEmpty()) {
			output = output + "<description>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.ds", occurrencesMap)) + "</description>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.p.y", occurrencesMap).isEmpty()) {
			output = output + "<lat>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.p.y", occurrencesMap)) + "</lat>";
		}
		if(!TransformationUtils.findNodeValue(nodes, "outputDataType.objs.p.x", occurrencesMap).isEmpty()) {
			output = output + "<lon>" + StringEscapeUtils.escapeXml(TransformationUtils.findNodeValue(nodes, "outputDataType.objs.p.x", occurrencesMap)) + "</lon>";
		}
		output = output + "</pois>";
					outputDataTypeobjsCounter++;
		}


		output = output + "</ns:poiResponse>";

		return MessageBuilder.withPayload(output).build();
	}
}
